cmake_minimum_required(VERSION 2.8.3)
project(vl_slam_loop_detection)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")

find_package(Eigen3 REQUIRED QUIET)
find_package(OpenCV 3 REQUIRED)
find_package(DLib REQUIRED QUIET)
find_package(DBoW2 REQUIRED QUIET)

LIST(APPEND CATKIN_DEPENDS_LIST
  cmake_modules
  cv_bridge
  image_transport
  linefit_ground_segmentation
  ndt_omp
  pcl_catkin
  pose_graph_utils
  roscpp
  segmatch
  segmatch_ros
  vl_slam_utils
)


find_package(catkin REQUIRED
  COMPONENTS
    ${CATKIN_DEPENDS_LIST}
)

catkin_package(
  LIBRARIES
    ${PROJECT_NAME}
  DEPENDS
    EIGEN3
    OpenCV
    DBoW2
    DLib
  INCLUDE_DIRS
    include
    ${OpenCV_INCLUDE_DIRS}
    ${DLib_INCLUDE_DIRS}
    ${DBoW2_INCLUDE_DIRS}
  CATKIN_DEPENDS
    ${CATKIN_DEPENDS_LIST}
)

include_directories(
  include
  SYSTEM
    ${OpenCV_INCLUDE_DIRS}
    ${catkin_INCLUDE_DIRS}
    ${DLib_INCLUDE_DIRS}
    ${DBoW2_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME} SHARED
  src/orb_extractor.cpp
  src/registration.cpp
  src/keyframe.cpp
  src/keyframe_database.cpp
  src/submap_loop_detector.cpp
  src/visual_loop_detector.cpp
  src/segmatch_loop_detector.cpp
)
target_link_libraries(${PROJECT_NAME} 
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  ${DLib_LIBS}
  ${DBoW2_LIBS}
)
