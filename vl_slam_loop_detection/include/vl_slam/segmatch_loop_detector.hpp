#ifndef SEGMATCH_LOOP_DETECTOR_HPP
#define SEGMATCH_LOOP_DETECTOR_HPP

#include <iostream>
#include <thread>

#include <ros/ros.h>

#include <ground_segmentation/ground_segmentation.h>

#include <segmatch/common.hpp>
#include <segmatch_ros/common.hpp>
#include <segmatch_ros/segmatch_worker.hpp>

#include "vl_slam/common.hpp"
#include "vl_slam/conversions.hpp"
#include "vl_slam/keyframe.hpp"
#include "vl_slam/keyframe_database.hpp"
#include "vl_slam/loop.hpp"
#include "vl_slam/registration.hpp"

namespace vl_slam {

/**
 * @brief this class finds loops by matching point cloud segments
 */
class SegMatchLoopDetector {
 public:
  /**
   * @brief constructor
   * @param pnh a private node handle
   * @param keyframe_db a pointer to the keyframe database
   */
  SegMatchLoopDetector(ros::NodeHandle& pnh, KeyframeDatabase::Ptr& keyframe_db);

  // TODO doc
  void init();

  // TODO doc
  void startThread();

  // TODO doc
  void staticGroundRemoval(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>& ground_seg_cloud) const;

  // TODO doc
  void dynamicGroundRemoval(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>& ground_seg_cloud) const;

  // TODO doc
  void removeGround(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud) const;

  // TODO doc
  void processNewKeyframe(const Keyframe::Ptr keyframe);

  // TODO doc
  void publishClouds(
      const pcl::PointCloud<PointI>::Ptr& source, const pcl::PointCloud<PointI>::Ptr& target, const pcl::PointCloud<PointI>::Ptr& aligned);

  bool performIcpAfterLoopClosure(
      const Keyframe::Ptr keyframe_a, const Keyframe::Ptr keyframe_b, const Eigen::Matrix4f guess, Eigen::Matrix4f& loop_transform);

  // TODO doc
  void segMatchThread();

  // TODO doc
  void updateTrajectory();

  // protected:
  const std::string detector_type_ = "segmatch";
  Eigen::MatrixXd inf_matrix_;
  std::vector<Loop::Ptr> new_loops_;
  std::vector<Loop::Ptr> detected_loops_;

  bool detection_thread_started_;
  KeyframeDatabase::Ptr keyframe_db_;

  std::string map_frame_id_;
  std::string odom_frame_id_;
  std::string robot_frame_id_;

  // Ground segmentation
  GroundSegmentationParams ground_seg_params_;
  bool dynamic_ground_removal_;
  double min_z_ground_;

  // SegMatch
  std::thread segmatch_thread_;
  segmatch_ros::SegMatchWorker segmatch_worker_;
  segmatch_ros::SegMatchWorkerParams segmatch_worker_params_;
  static constexpr double kSegMatchSleepTime_s = 0.01;
  std::vector<segmatch::LocalMap<segmatch::PclPoint, segmatch::MapPoint>> local_maps_;
  std::vector<PointCloud> local_map_queue_;
  std::mutex local_map_mutex_;
  laser_slam::SE3 pose_at_last_localization_;
  bool pose_at_last_localization_set_ = false;
  bool first_points_received_;
  bool clear_local_map_after_loop_closure_;
  bool trajectory_updated_;

  // Extra matching step on loop closures
  bool do_icp_step_on_loop_closures_;
  double icp_fitness_thresh_;  // threshold for scan matching
  pcl::Registration<PointI, PointI>::Ptr registration_;

  bool publish_aligned_clouds_;
  ros::Publisher source_cloud_pub_;
  ros::Publisher target_cloud_pub_;
  ros::Publisher aligned_cloud_pub_;

  ros::Publisher ground_seg_cloud_pub_;
  ros::Publisher local_map_cloud_pub_;
};

}  // namespace vl_slam

#endif  // SEGMATCH_LOOP_DETECTOR_HPP
