#ifndef LOOP_HPP
#define LOOP_HPP

#include <iostream>

#include <ros/ros.h>

#include "vl_slam/common.hpp"
#include "vl_slam/keyframe.hpp"

namespace vl_slam {

struct Loop {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  using Ptr = std::shared_ptr<Loop>;

  Loop(const Keyframe::Ptr& keyframe_a, const Keyframe::Ptr& keyframe_b, const Eigen::Matrix4f& rel_pose)
      : keyframe_a_(keyframe_a), keyframe_b_(keyframe_b), rel_pose_(rel_pose), inf_matrix_(Eigen::MatrixXd::Identity(6, 6)) {}

  Loop(
      const Keyframe::Ptr& keyframe_a, const Keyframe::Ptr& keyframe_b, const Eigen::Matrix4f& rel_pose, const std::string detector_type,
      const Eigen::MatrixXd& inf_matrix)
      : detector_type_(detector_type), keyframe_a_(keyframe_a), keyframe_b_(keyframe_b), rel_pose_(rel_pose), inf_matrix_(inf_matrix) {}

  Eigen::Isometry3d estimateTransform() {
    Eigen::Isometry3d w_T_a_b = Eigen::Isometry3d(rel_pose_.cast<double>());
    Eigen::Isometry3d T_w_a = keyframe_a_->node_->estimate();
    Eigen::Isometry3d T_w_b = keyframe_b_->node_->estimate();
    Eigen::Isometry3d a_T_a_b = T_w_a.inverse() * w_T_a_b * T_w_b;

    return a_T_a_b;
  }

  std::string detector_type_;

  ros::Time stamp_;

  Keyframe::Ptr keyframe_a_;
  Keyframe::Ptr keyframe_b_;

  Eigen::Matrix4f rel_pose_;  // a_T_a_b
  Eigen::MatrixXd inf_matrix_;
};

}  // namespace vl_slam

#endif  // LOOP_HPP