#ifndef LOOP_DETECTION_UTILS_HPP_
#define LOOP_DETECTION_UTILS_HPP_

#include "vl_slam/common.hpp"

#include <Eigen/Dense>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <segmatch/common.hpp>
#include <segmatch_ros/common.hpp>
#include <segmatch_ros/segmatch_worker.hpp>

namespace vl_slam {

laser_slam::Pose convertEigenToPose(const double& time_ns, const Eigen::Vector3d& trans, const Eigen::Quaterniond& rot) {
  laser_slam::Pose pose;
  pose.time_ns = time_ns;
  pose.T_w.getPosition().x() = trans.x();
  pose.T_w.getPosition().y() = trans.y();
  pose.T_w.getPosition().z() = trans.z();
  pose.T_w.getRotation().setValues(rot.w(), rot.x(), rot.y(), rot.z());
  return pose;
}

laser_slam::Pose convertEigenToPose(const double& time_ns, const Eigen::Isometry3d& transform) {
  Eigen::Vector3d pos = transform.translation();
  Eigen::Quaterniond rot(transform.rotation());
  return convertEigenToPose(time_ns, pos, rot);
}

laser_slam::SE3 convertEigenToSE3(const Eigen::Vector3d& trans, const Eigen::Quaterniond& rot) {
  laser_slam::SE3 T;
  T.getPosition().x() = trans.x();
  T.getPosition().y() = trans.y();
  T.getPosition().z() = trans.z();
  T.getRotation().setValues(rot.w(), rot.x(), rot.y(), rot.z());
  return T;
}

laser_slam::SE3 convertEigenToSE3(const Eigen::Isometry3d& transform) {
  Eigen::Vector3d pos = transform.translation();
  Eigen::Quaterniond rot(transform.rotation());
  return convertEigenToSE3(pos, rot);
}

}  // namespace vl_slam
#endif