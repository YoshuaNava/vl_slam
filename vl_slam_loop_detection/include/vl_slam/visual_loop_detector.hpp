#ifndef VISUAL_LOOP_DETECTOR_HPP
#define VISUAL_LOOP_DETECTOR_HPP

#include <image_transport/image_transport.h>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc.hpp"

#include "vl_slam/loop_detector_base.hpp"

namespace vl_slam {

/**
 * @brief this class finds loops by matching visual bag of words representations of images
 */
class VisualLoopDetector : public LoopDetectorBase {
 public:
  /**
   * @brief constructor
   * @param pnh
   */
  VisualLoopDetector(ros::NodeHandle& pnh, KeyframeDatabase::Ptr& keyframe_db);

  // TODO doc
  void startThread();

  // TODO doc
  void processImage(const cv::Mat& img);

  // TODO doc
  double getInliersRatio(const Keyframe::Ptr& keyframe_a, const Keyframe::Ptr& keyframe_b);

  /**
   * @brief detect loops and add them to the pose graph
   */
  std::vector<Loop::Ptr> detect();

  /**
   * @brief detect loops and add them to the pose graph
   * @param keyframes       keyframes
   * @param new_keyframes   newly registered keyframes
   */
  std::vector<Loop::Ptr> detect(const std::vector<Keyframe::Ptr>& keyframes, const std::vector<Keyframe::Ptr>& new_keyframes);

  // protected:

  // TODO doc
  void loadOrbVocabulary();

  // TODO doc
  bool estimateRigidTransform(
      const pcl::PointCloud<PointI>::Ptr& source_cloud, const pcl::PointCloud<PointI>::Ptr& target_cloud,
      const pcl::PointCloud<PointI>::Ptr& aligned_cloud, const Eigen::Matrix4f& guess, Eigen::Matrix4f& T);

  // TODO doc
  void visualizeImagesThread();

  std::pair<Keyframe::Ptr, Keyframe::Ptr> matched_keyframes_;
  bool new_image_;

  int features_per_img_;
  PinholeCamera::Ptr camera_;
  std::string visual_vocab_path_;
  std::shared_ptr<OrbVocabulary> orb_vocabulary_;
  std::shared_ptr<OrbExtractor> orb_extractor_;
  std::thread img_vis_thread_;
  std::mutex cv_window_mutex_;

  double inliers_ratio_thresh_;
  double ransac_thresh_;
  double nn_matching_ratio_;

  image_transport::Publisher current_kf_pub_;
  image_transport::Publisher matched_kfs_pub_;
};

}  // namespace vl_slam

#endif  // VISUAL_LOOP_DETECTOR_HPP
