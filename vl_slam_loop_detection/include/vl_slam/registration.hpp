#ifndef REGISTRATION_HPP
#define REGISTRATION_HPP

#include <pcl/registration/registration.h>

#include <ros/ros.h>

namespace vl_slam {

/**
 * @brief Select a scan matching algorithm according to rosparams
 * @param nh ROS NodeHandle.
 * @return Selected scan matching method.
 */
boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI>> select_registration_method(ros::NodeHandle& nh);

}  // namespace vl_slam

#endif  //
