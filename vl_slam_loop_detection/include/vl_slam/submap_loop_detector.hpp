#ifndef SUBMAP_LOOP_DETECTOR_HPP
#define SUBMAP_LOOP_DETECTOR_HPP

#include "vl_slam/loop_detector_base.hpp"

namespace vl_slam {

/**
 * @brief this class finds loops by scan matching
 */
class SubmapLoopDetector : public LoopDetectorBase {
 public:
  /**
   * @brief constructor
   * @param pnh
   */
  SubmapLoopDetector(ros::NodeHandle& nh, KeyframeDatabase::Ptr& keyframe_db);

  /**
   * @brief detect loops and add them to the pose graph
   * @param keyframes       keyframes
   * @param new_keyframes   newly registered keyframes
   */
  std::vector<Loop::Ptr> detect();

 protected:
  /**
   * @brief To validate a loop candidate this function applies a scan matching between keyframes consisting the loop. If they are matched
   * well, the loop keys are returned
   * @param candidate_keyframes  candidate keyframes of loop start
   * @param new_keyframe         loop end keyframe
   */
  Loop::Ptr matching(const std::vector<Keyframe::Ptr>& candidate_keyframes, const Keyframe::Ptr& new_keyframe);

 protected:
  std::pair<Keyframe::Ptr, Keyframe::Ptr> matched_keyframes_;
};

}  // namespace vl_slam

#endif  // SUBMAP_LOOP_DETECTOR_HPP
