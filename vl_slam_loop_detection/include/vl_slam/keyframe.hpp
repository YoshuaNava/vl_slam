#ifndef KEYFRAME_HPP
#define KEYFRAME_HPP

#include <boost/optional.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <DBoW2.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <g2o/types/slam3d/vertex_se3.h>

#include <ros/ros.h>

#include "vl_slam/camera.hpp"
#include "vl_slam/common.hpp"
#include "vl_slam/orb_extractor.hpp"

namespace g2o {
class VertexSE3;
}

namespace vl_slam {

#define FRAME_GRID_ROWS 48
#define FRAME_GRID_COLS 64

/**
 * @brief Keyframe (pose node)
 */
struct Keyframe {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  using PointI = pcl::PointXYZI;
  using Ptr = std::shared_ptr<Keyframe>;

  Keyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, double accum_distance);

  Keyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, double accum_distance, const pcl::PointCloud<PointI>::Ptr& cloud);

  // TODO doc
  bool processImage(
      const cv::Mat& img, const std::shared_ptr<OrbVocabulary>& orb_vocabulary, const std::shared_ptr<OrbExtractor>& orb_extractor,
      const PinholeCamera::Ptr& camera);

  // TODO doc
  // Assign keypoints to the grid for speed up feature matching (called in the constructor).
  void assignFeaturesToGrid();

  // TODO doc
  bool posInGrid(const cv::KeyPoint& kp, int& pos_x, int& pos_y);

  // TODO doc
  void computeImageBounds(const cv::Mat& imLeft);

  // TODO doc
  void undistortKeyPoints();

  // TODO doc
  void computeBoW();

  // TODO doc
  std::vector<cv::Mat> toDescriptorVector(const cv::Mat& descriptors);

  // TODO doc
  void dump(const std::string& directory);

 public:
  ros::Time stamp_;  // timestamp

  Eigen::Isometry3d T_odom_;  // T_odometry (estimated by loam)
  g2o::VertexSE3* node_;      // g2o node instance
  double accum_distance_;     // accumulated distance from the first node (by scan_matching_T_odometry)

  bool has_cloud_;
  pcl::PointCloud<PointI>::Ptr cloud_;  // point cloud

  bool has_img_;
  cv::Mat img_;

  ///////////////////////////////    VISUAL    ///////////////////////////////
  // Calibration matrix and OpenCV distortion parameters.
  cv::Mat K_;
  PinholeCamera::Ptr camera_;

  // Undistorted Image Bounds (computed once).
  static float img_min_x_;
  static float img_max_x_;
  static float img_min_y_;
  static float img_max_y_;
  static bool initial_run;

  // Visual feature matching
  // Keypoints are assigned to cells in a grid to reduce matching complexity when projecting MapPoints.
  static float grid_element_width_inv_;
  static float grid_element_height_inv_;
  std::vector<std::size_t> kpts_grid_[FRAME_GRID_COLS][FRAME_GRID_ROWS];
  ////////////////////////////////////////////////////////////////////////////

  // ORB extractor parameters
  int scale_levels_;
  float scale_factor_;
  float log_scale_factor_;
  std::vector<float> scale_factors_;
  std::vector<float> inv_scale_factors_;
  std::vector<float> level_sigma2_;
  std::vector<float> inv_level_sigma2_;

  // Visual feature detection
  std::vector<cv::KeyPoint> fast_kpts_;
  std::vector<cv::KeyPoint> fast_kpts_undist_;
  cv::Mat orb_descriptors_;
  int num_features_;
  DBoW2::FeatureVector feat_vec_;
  DBoW2::BowVector vBoW_vec_;
  std::shared_ptr<OrbVocabulary> orb_vocabulary_;
};

/**
 * @brief Keyframesnapshot for map cloud generation
 */
struct KeyframeSnapshot {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using Ptr = std::shared_ptr<KeyframeSnapshot>;

  KeyframeSnapshot(const Keyframe::Ptr& keyframe);
  KeyframeSnapshot(const Eigen::Isometry3d& T_map, const pcl::PointCloud<PointI>::Ptr& cloud);

  ~KeyframeSnapshot();

 public:
  Eigen::Isometry3d T_map_;             // pose estimated by graph optimization
  pcl::PointCloud<PointI>::Ptr cloud_;  // point cloud
};

}  // namespace vl_slam

#endif  // KEYFRAME_HPP