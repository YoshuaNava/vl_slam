#ifndef LOOP_DETECTOR_BASE_HPP
#define LOOP_DETECTOR_BASE_HPP

#include <thread>

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include "vl_slam/common.hpp"
#include "vl_slam/keyframe.hpp"
#include "vl_slam/keyframe_database.hpp"
#include "vl_slam/loop.hpp"
#include "vl_slam/registration.hpp"

namespace vl_slam {

/**
 * @brief this class finds loops
 */
class LoopDetectorBase {
 public:
  /**
   * @brief constructor
   * @param pnh
   */
  LoopDetectorBase(std::string detector_type) : detector_type_(detector_type), inf_matrix_(Eigen::MatrixXd::Identity(6, 6)) {}

  void setup(ros::NodeHandle& nh, KeyframeDatabase::Ptr& keyframe_db, const std::string module_name) {
    const std::string ns = "vl_slam/";

    // Robot frames
    map_frame_id_ = nh.param<std::string>(ns + "map_frame_id", "map");
    odom_frame_id_ = nh.param<std::string>(ns + "odom_frame_id", "odom");
    robot_frame_id_ = nh.param<std::string>(ns + "robot_frame_id", "camera");

    // Load information matrix
    std::vector<float> inf_matrix_vector;
    nh.getParam(ns + module_name + "information_matrix", inf_matrix_vector);
    for (size_t i = 0; i < inf_matrix_vector.size(); i++) {
      inf_matrix_(i, i) = inf_matrix_vector[i];
    }

    // Local thresholding on new candidates
    apply_thresholds_candidates_ = nh.param<bool>(ns + module_name + "apply_thresholds_candidates", true);
    distance_thresh_ = nh.param<double>(ns + module_name + "distance_thresh", 5.0);
    accum_distance_thresh_ = nh.param<double>(ns + module_name + "accum_distance_thresh", 8.0);
    distance_from_last_edge_thresh_ = nh.param<double>(ns + module_name + "min_edge_interval", 5.0);
    last_edge_accum_distance_ = 0.0;

    // Matching
    fitness_score_thresh_ = nh.param<double>(ns + module_name + "fitness_score_thresh", 20.0);
    registration_ = select_registration_method(nh);

    // Debugging - Visualization markers
    publish_markers_ = nh.param<bool>(ns + module_name + "publish_markers", false);
    publish_predicted_segment_matches_ = nh.param<bool>(ns + module_name + "publish_predicted_segment_matches", false);
    line_scale_matches_ = nh.param<double>(ns + module_name + "line_scale_matches", 1.0);
    line_scale_loop_closures_ = nh.param<double>(ns + module_name + "line_scale_loop_closures", 5.0);
    if (!nh.getParam(ns + module_name + "markers_color", markers_color_)) {
      markers_color_ = {0.7, 0.7, 0.7, 1.0};
    }
    loops_markers_pub_ = nh.advertise<visualization_msgs::Marker>(ns + module_name + "loops_markers", 1, true);
    sphere_marker_pub_ = nh.advertise<visualization_msgs::Marker>(ns + module_name + "sphere_marker", 1, true);

    // Debugging - Source/Target/Aligned clouds
    publish_aligned_clouds_ = nh.param<bool>(ns + module_name + "publish_aligned_clouds", false);
    if (publish_aligned_clouds_) {
      source_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "source_cloud", 1, true);
      target_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "target_cloud", 1, true);
      aligned_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "aligned_cloud", 1, true);
    }

    markers_update_interval_ = nh.param<double>(ns + "markers_update_interval", 0.15);
    markers_publish_timer_ = nh.createTimer(ros::Duration(markers_update_interval_), &LoopDetectorBase::publishMarkersTimerCallback, this);

    keyframe_db_ = keyframe_db;
  }

  double getDistanceThresh() const {
    return distance_thresh_;
  }

  void loopDetectionThread();

  /**
   * @brief detect loops and add them to the pose graph
   */
  // virtual std::vector<Loop::Ptr> detect() = 0;

  /**
   * @brief detect loops and add them to the pose graph
   * @param keyframes       keyframes
   * @param new_keyframes   newly registered keyframes
   */
  std::vector<Loop::Ptr> detect(const std::vector<Keyframe::Ptr>& keyframes, const std::vector<Keyframe::Ptr>& new_keyframes);

  /**
   * @brief find loop candidates. A detected loop begins at one of #keyframes and ends at #new_keyframe
   * @param keyframes      candidate keyframes of loop start
   * @param new_keyframe   loop end keyframe
   * @return loop candidates
   */
  virtual std::vector<Keyframe::Ptr> findCloseCandidates(const std::vector<Keyframe::Ptr>& keyframes, const Keyframe::Ptr& new_keyframe) {
    // too close to the last registered loop edge
    if (new_keyframe->accum_distance_ - last_edge_accum_distance_ < distance_from_last_edge_thresh_) {
      return std::vector<Keyframe::Ptr>();
    }

    std::vector<Keyframe::Ptr> candidates;
    candidates.reserve(32);

    size_t num_keyframes = keyframes.size();
    for (size_t i = 0; i < num_keyframes; i++) {
      Keyframe::Ptr k = keyframes[i];
      if (new_keyframe->stamp_.toSec() > k->stamp_.toSec()) {
        // traveled distance between keyframes is too small
        if (new_keyframe->accum_distance_ - k->accum_distance_ < accum_distance_thresh_) {
          continue;
        }

        const auto& pos1 = k->node_->estimate().translation();
        const auto& pos2 = new_keyframe->node_->estimate().translation();

        // estimated distance between keyframes is too large
        double dist = (pos1.head<3>() - pos2.head<3>()).norm();
        if (dist > distance_thresh_) {
          continue;
        }

        candidates.push_back(k);
        // std::cout<< "   Accum distance " << new_keyframe->accum_distance_ - k->accum_distance_ << std::endl;
        // std::cout<< "   Distance " << dist << std::endl;
      }
    }
    return candidates;
  }

  /**
   * @brief To validate a loop candidate this function applies a scan matching between keyframes consisting the loop. If they are matched
   * well, the loop keys are returned
   * @param candidate_keyframes  candidate keyframes of loop start
   * @param new_keyframe         loop end keyframe
   */
  Loop::Ptr matching(const std::vector<Keyframe::Ptr>& candidate_keyframes, const Keyframe::Ptr& new_keyframe);

  // TODO doc
  void publishMarkersTimerCallback(const ros::TimerEvent& event) {
    if (!keyframe_db_->getNumberKeyframes())
      return;

    // Loop closure edges
    if (loops_markers_pub_.getNumSubscribers()) {
      visualization_msgs::Marker loop_closures_marker;
      loop_closures_marker.header.frame_id = map_frame_id_;
      loop_closures_marker.header.stamp = ros::Time::now();
      loop_closures_marker.id = 0;
      loop_closures_marker.ns = detector_type_;
      loop_closures_marker.type = visualization_msgs::Marker::LINE_LIST;
      loop_closures_marker.action = visualization_msgs::Marker::ADD;
      loop_closures_marker.color.r = markers_color_[0];
      loop_closures_marker.color.g = markers_color_[1];
      loop_closures_marker.color.b = markers_color_[2];
      loop_closures_marker.color.a = markers_color_[3];
      loop_closures_marker.scale.x = line_scale_loop_closures_;
      for (size_t i = 0; i < detected_loops_.size(); i++) {
        Loop::Ptr loop = detected_loops_[i];

        geometry_msgs::Point p;
        p.x = loop->keyframe_a_->node_->estimate().translation().x();
        p.y = loop->keyframe_a_->node_->estimate().translation().y();
        p.z = loop->keyframe_a_->node_->estimate().translation().z();
        loop_closures_marker.points.push_back(p);

        p.x = loop->keyframe_b_->node_->estimate().translation().x();
        p.y = loop->keyframe_b_->node_->estimate().translation().y();
        p.z = loop->keyframe_b_->node_->estimate().translation().z();
        loop_closures_marker.points.push_back(p);
      }

      loops_markers_pub_.publish(loop_closures_marker);
    }

    // Loop closure sphere (distance threshold representation)
    if (apply_thresholds_candidates_ && sphere_marker_pub_.getNumSubscribers()) {
      visualization_msgs::Marker sphere_marker;
      sphere_marker.header.frame_id = map_frame_id_;
      sphere_marker.header.stamp = ros::Time::now();
      sphere_marker.id = 0;
      sphere_marker.ns = detector_type_;
      sphere_marker.type = visualization_msgs::Marker::SPHERE;

      Eigen::Vector3d pos = keyframe_db_->getLatestKeyframe()->node_->estimate().translation();
      sphere_marker.pose.position.x = pos.x();
      sphere_marker.pose.position.y = pos.y();
      sphere_marker.pose.position.z = pos.z();

      sphere_marker.pose.orientation.w = 1.0;
      sphere_marker.scale.x = distance_thresh_ * 2.0;
      sphere_marker.scale.y = sphere_marker.scale.x;
      sphere_marker.scale.z = sphere_marker.scale.x;
      sphere_marker.color.r = markers_color_[0];
      sphere_marker.color.g = markers_color_[1];
      sphere_marker.color.b = markers_color_[2];
      sphere_marker.color.a = 0.6;

      sphere_marker_pub_.publish(sphere_marker);
    }
  }

  void publishClouds(
      const pcl::PointCloud<PointI>::Ptr& source, const pcl::PointCloud<PointI>::Ptr& target, const pcl::PointCloud<PointI>::Ptr& aligned) {
    publishPointCloud(source, map_frame_id_, ros::Time::now(), &source_cloud_pub_);
    publishPointCloud(target, map_frame_id_, ros::Time::now(), &target_cloud_pub_);
    publishPointCloud(aligned, map_frame_id_, ros::Time::now(), &aligned_cloud_pub_);
  }

  // protected:
  const std::string detector_type_ = "loop_detector_base";
  Eigen::MatrixXd inf_matrix_;
  std::vector<Loop::Ptr> candidate_loops_;
  std::vector<Loop::Ptr> detected_loops_;
  std::vector<Loop::Ptr> new_loops_;

  bool detection_thread_started_;
  std::thread detection_thread_;
  KeyframeDatabase::Ptr keyframe_db_;

  bool apply_thresholds_candidates_;
  double distance_thresh_;                 // estimated distance between keyframes consisting a loop must be less than this distance
  double accum_distance_thresh_;           // traveled distance between ...
  double distance_from_last_edge_thresh_;  // a new loop edge must far from the last one at least this distance
  double last_edge_accum_distance_;        // TODO doc

  double fitness_score_thresh_;  // threshold for scan matching
  pcl::Registration<PointI, PointI>::Ptr registration_;

  std::string map_frame_id_;
  std::string odom_frame_id_;
  std::string robot_frame_id_;

  bool publish_markers_;
  double markers_update_interval_;
  std::vector<float> markers_color_;
  double line_scale_matches_;
  double line_scale_loop_closures_;
  ros::Publisher loops_markers_pub_;
  ros::Publisher sphere_marker_pub_;
  ros::Timer markers_publish_timer_;

  bool publish_predicted_segment_matches_;
  bool publish_aligned_clouds_;
  ros::Publisher source_cloud_pub_;
  ros::Publisher target_cloud_pub_;
  ros::Publisher aligned_cloud_pub_;
};

}  // namespace vl_slam

#endif  // LOOP_DETECTOR_BASE_HPP
