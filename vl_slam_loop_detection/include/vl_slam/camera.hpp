#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <opencv2/core.hpp>

namespace vl_slam {

class PinholeCamera {
 public:
  using Ptr = std::shared_ptr<PinholeCamera>;

  PinholeCamera(const cv::Mat& K, const cv::Mat& dist_coeffs, const int& img_width, const int& img_height) {
    fx_ = K.at<float>(0, 0);
    fy_ = K.at<float>(1, 1);
    cx_ = K.at<float>(0, 2);
    cy_ = K.at<float>(1, 2);
    inv_fx_ = 1.0 / fx_;
    inv_fy_ = 1.0 / fy_;
    img_width_ = img_width;
    img_height_ = img_height;
    K_ = K.clone();
    dist_coeffs_ = dist_coeffs.clone();
  }

  // protected:

  cv::Mat K_;
  double fx_;
  double fy_;
  double cx_;
  double cy_;
  double inv_fx_;
  double inv_fy_;
  cv::Mat dist_coeffs_;

  int img_width_;
  int img_height_;
};

}  // namespace vl_slam

#endif  // CAMERA_HPP