#ifndef KEYFRAME_DATABASE_HPP
#define KEYFRAME_DATABASE_HPP

#include <mutex>

#include <Eigen/Dense>
#include <g2o/types/slam3d/vertex_se3.h>
#include <ros/ros.h>

#include "vl_slam/common.hpp"
#include "vl_slam/keyframe.hpp"
#include "vl_slam/loop.hpp"

namespace vl_slam {

/**
 * @brief this class decides if a new frame should be registered to the pose graph as a keyframe
 */
class KeyframeDatabase {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  using Ptr = std::shared_ptr<KeyframeDatabase>;

  /**
   * @brief Constructor
   * @param pnh Private node handle
   */
  KeyframeDatabase(ros::NodeHandle& pnh);

  /**
   * @brief decide if a new frame should be registered to the graph
   * @param pose  pose of the frame
   * @return  if true, the frame should be registered
   */
  bool thresholdNewPose(const Eigen::Isometry3d& pose);

  /**
   * @brief the last keyframe's accumulated distance from the first keyframe
   * @return accumulated distance
   */
  double getAccumDistance() const;

  // TODO doc
  Eigen::Isometry3d getTransformOdomToMap();

  // TODO doc
  Eigen::Isometry3d getLatestPose();

  // TODO doc
  void updateOdomToMapTransform(Eigen::Isometry3d& T_odom2map, ros::Time& stamp);

  // TODO doc
  void addNewKeyframe(const Keyframe::Ptr& keyframe);

  // TODO doc
  void addNewKeyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, const pcl::PointCloud<PointI>::Ptr& cloud);

  // TODO doc
  std::vector<Keyframe::Ptr>& getNewKeyframes();

  // TODO doc
  std::vector<Keyframe::Ptr> getNewKeyframesCopy();

  // TODO doc
  bool clearNewKeyframes();

  // TODO doc
  size_t getNumberKeyframes();

  // TODO doc
  std::vector<Keyframe::Ptr> getKeyframeList();

  // TODO doc
  std::vector<Keyframe::Ptr>& getKeyframes();

  // TODO doc
  std::vector<Keyframe::Ptr> getKeyframesCopy();

  // TODO doc
  std::vector<KeyframeSnapshot::Ptr> takeKeyframesSnapshot();

  // TODO doc
  Keyframe::Ptr getKeyframe(size_t idx);

  // TODO doc
  Keyframe::Ptr getLatestKeyframe();

  // TODO doc
  size_t getNumberLoops();

  // TODO doc
  std::vector<Loop::Ptr> getLoopList();

  // TODO doc
  Loop::Ptr getLoop(size_t idx);

  // TODO doc
  void addNewLoops(const std::vector<Loop::Ptr> new_loops);

  // TODO doc
  double rosTimeToCurveTime(const double& timestamp_ns);

  // TODO doc
  double curveTimeToRosTime(const double& timestamp_ns) const;

  // TODO doc
  bool findKeyframeByTimestamp(const double& timestamp_ns, Keyframe::Ptr& kf);

  // protected:
  // Basic state params
  bool is_first_;
  Eigen::Matrix4f T_odom2map_;
  Eigen::Isometry3d prev_keypose_;

  // Mutexes
  std::mutex T_odom2map_mutex_;
  std::mutex keyframes_mutex_;
  std::mutex loops_mutex_;
  std::mutex keyframes_snapshot_mutex_;

  // Keyframe creation thresholds
  double accum_distance_;
  double keyframe_delta_trans_;
  double keyframe_delta_angle_;
  bool keyframe_store_clouds_;

  // Timestamp to be subtracted to each measurement time so that the trajectory starts at time 0.
  double base_time_ns_ = 0;
  // Indicates whether the base time was set.
  bool base_time_set_ = false;

  std::vector<Keyframe::Ptr> keyframes_;
  std::vector<Keyframe::Ptr> new_keyframes_;
  std::vector<KeyframeSnapshot::Ptr> keyframes_snapshot_;
  std::vector<Loop::Ptr> loops_;
};

}  // namespace vl_slam

#endif  // KEYFRAME_DATABASE_HPP
