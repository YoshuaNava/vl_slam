
#include "vl_slam/registration.hpp"

#include <iostream>

#include <pcl/registration/gicp.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/ndt.h>
#include <pclomp/gicp_omp.h>
#include <pclomp/ndt_omp.h>

namespace vl_slam {

boost::shared_ptr<pcl::Registration<pcl::PointXYZI, pcl::PointXYZI>> select_registration_method(ros::NodeHandle& nh) {
  using PointI = pcl::PointXYZI;
  const std::string ns = "vl_slam/";
  const std::string module_name = "submap_detector/";

  // select a registration method (ICP, GICP, NDT)
  const std::string registration_method = nh.param<std::string>(ns + module_name + "registration_method", "NDT_OMP");
  if (registration_method == "ICP") {
    std::cout << "registration: ICP" << std::endl;
    boost::shared_ptr<pcl::IterativeClosestPoint<PointI, PointI>> icp(new pcl::IterativeClosestPoint<PointI, PointI>());
    return icp;
  } else if (registration_method.find("GICP") != std::string::npos) {
    if (registration_method.find("OMP") == std::string::npos) {
      std::cout << "registration: GICP" << std::endl;
      boost::shared_ptr<pcl::GeneralizedIterativeClosestPoint<PointI, PointI>> gicp(
          new pcl::GeneralizedIterativeClosestPoint<PointI, PointI>());
      gicp->setMaximumIterations(20);
      gicp->setTransformationEpsilon(1e-06);
      gicp->setMaxCorrespondenceDistance(1.0);
      gicp->setRANSACIterations(0);
      return gicp;
    } else {
      std::cout << "registration: GICP_OMP" << std::endl;
      boost::shared_ptr<pclomp::GeneralizedIterativeClosestPoint<PointI, PointI>> gicp(
          new pclomp::GeneralizedIterativeClosestPoint<PointI, PointI>());
      return gicp;
    }
  } else {
    if (registration_method.find("NDT") == std::string::npos) {
      std::cerr << "warning: unknown registration type(" << registration_method << ")" << std::endl;
      std::cerr << "       : use NDT" << std::endl;
    }

    double ndt_resolution = nh.param<double>(ns + module_name + "ndt_resolution", 1.0);
    if (registration_method.find("OMP") == std::string::npos) {
      std::cout << "registration: NDT " << ndt_resolution << std::endl;
      boost::shared_ptr<pcl::NormalDistributionsTransform<PointI, PointI>> ndt(new pcl::NormalDistributionsTransform<PointI, PointI>());
      ndt->setTransformationEpsilon(0.01);
      ndt->setResolution(ndt_resolution);
      return ndt;
    } else {
      int num_threads = nh.param<int>(ns + module_name + "ndt_num_threads", 0);
      std::string nn_search_method = nh.param<std::string>(ns + module_name + "ndt_nn_search_method", "DIRECT7");
      std::cout << "Loop detection registration: NDT_OMP " << nn_search_method << " with res. " << ndt_resolution << " and (" << num_threads
                << " threads)" << std::endl;
      boost::shared_ptr<pclomp::NormalDistributionsTransform<PointI, PointI>> ndt(
          new pclomp::NormalDistributionsTransform<PointI, PointI>());
      if (num_threads > 0) {
        ndt->setNumThreads(num_threads);
      }
      ndt->setTransformationEpsilon(0.01);
      ndt->setMaximumIterations(32);
      ndt->setResolution(ndt_resolution);
      if (nn_search_method == "KDTREE") {
        ndt->setNeighborhoodSearchMethod(pclomp::KDTREE);
      } else if (nn_search_method == "DIRECT1") {
        ndt->setNeighborhoodSearchMethod(pclomp::DIRECT1);
      } else {
        ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
      }
      return ndt;
    }
  }

  return nullptr;
}

}  // namespace vl_slam
