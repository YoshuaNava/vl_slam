
#include "vl_slam/visual_loop_detector.hpp"

#include <iostream>

#include <cv_bridge/cv_bridge.h>

#include <g2o/types/slam3d/vertex_se3.h>

#include <sensor_msgs/PointCloud2.h>

namespace vl_slam {

VisualLoopDetector::VisualLoopDetector(ros::NodeHandle& nh, KeyframeDatabase::Ptr& keyframe_db)
    : LoopDetectorBase("visualBoW"), new_image_(false) {
  const std::string ns = "vl_slam/";
  const std::string module_name = "visual_detector/";
  LoopDetectorBase::setup(nh, keyframe_db, module_name);

  image_transport::ImageTransport it(nh);
  visual_vocab_path_ = nh.param<std::string>(ns + module_name + "visual_vocab_path", std::string(""));
  features_per_img_ = nh.param<int>(ns + module_name + "features_per_img", 500);

  ransac_thresh_ = nh.param<int>(ns + module_name + "ransac_thresh", 3.0);
  nn_matching_ratio_ = nh.param<double>(ns + module_name + "nn_matching_ratio", 0.8);
  inliers_ratio_thresh_ = nh.param<double>(ns + module_name + "inliers_ratio_thresh", 0.6);

  cv::Mat K = (cv::Mat_<double>(3, 3) << 707.0912, 0, 601.8873, 0, 707.0912, 183.1104, 0, 0, 1);
  cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, CV_32F);
  int img_width = 1392;
  int img_height = 512;
  camera_.reset(new PinholeCamera(K, dist_coeffs, img_width, img_height));

  loadOrbVocabulary();
  orb_extractor_.reset(new OrbExtractor(features_per_img_, 1.2, 8, 20, 7));

  matched_kfs_pub_ = it.advertise(ns + module_name + "matched_kfs", 1);
  current_kf_pub_ = it.advertise(ns + module_name + "current_kf", 1);

  matched_keyframes_.first = nullptr;
  matched_keyframes_.second = nullptr;
}

void VisualLoopDetector::startThread() {
  img_vis_thread_ = std::thread(&VisualLoopDetector::visualizeImagesThread, this);
}

void VisualLoopDetector::processImage(const cv::Mat& img) {
  if (keyframe_db_->getNumberKeyframes()) {
    Keyframe::Ptr latest_keyframe = keyframe_db_->getLatestKeyframe();
    if (!latest_keyframe->has_img_) {
      latest_keyframe->processImage(img, orb_vocabulary_, orb_extractor_, camera_);
      new_image_ = true;

      // If a new frame was added to the database, detect loops
      detect();
    }
  }
}

void VisualLoopDetector::loadOrbVocabulary() {
  std::cout << "Loading ORB Vocabulary. This could take a while..." << std::endl;
  clock_t tStart = clock();
  orb_vocabulary_.reset(new OrbVocabulary());
  bool bin_voc_load;  // chose loading method based on file extension
  if (hasSuffix(visual_vocab_path_, ".txt"))
    bin_voc_load = orb_vocabulary_->loadFromTextFile(visual_vocab_path_);
  else
    bin_voc_load = orb_vocabulary_->loadFromBinaryFile(visual_vocab_path_);

  if (!bin_voc_load) {
    std::cerr << "Wrong path to vocabulary. " << std::endl;
    std::cerr << "Failed to open file at: " << visual_vocab_path_ << std::endl;
    exit(-1);
  }
  printf("ORB Vocabulary loaded in %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
}

std::vector<Loop::Ptr> VisualLoopDetector::detect() {
  std::vector<Keyframe::Ptr> kfs_snapshot = keyframe_db_->getKeyframesCopy();
  std::vector<Keyframe::Ptr> new_kfs_snapshot = keyframe_db_->getNewKeyframesCopy();
  keyframe_db_->clearNewKeyframes();

  new_loops_ = detect(kfs_snapshot, new_kfs_snapshot);
  std::copy(new_loops_.begin(), new_loops_.end(), std::back_inserter(detected_loops_));

  // candidate_loops_.clear();

  return new_loops_;
}

bool VisualLoopDetector::estimateRigidTransform(
    const pcl::PointCloud<PointI>::Ptr& source_cloud, const pcl::PointCloud<PointI>::Ptr& target_cloud,
    const pcl::PointCloud<PointI>::Ptr& aligned_cloud, const Eigen::Matrix4f& guess, Eigen::Matrix4f& T) {
  registration_->setInputSource(source_cloud);
  registration_->setInputTarget(target_cloud);
  registration_->align(*aligned_cloud);

  T = registration_->getFinalTransformation();

  if (registration_->getFitnessScore() > fitness_score_thresh_) {
    std::cout << "Registration fitness score of loop too low" << std::endl;
    return false;
  }

  return true;
}

double VisualLoopDetector::getInliersRatio(const Keyframe::Ptr& keyframe_a, const Keyframe::Ptr& keyframe_b) {
  size_t num_inliers, num_matches, num_valid_matches;
  double inliers_ratio, matches_ratio;
  cv::BFMatcher matcher(cv::NORM_HAMMING);
  std::vector<std::vector<cv::DMatch>> matches;
  std::vector<cv::KeyPoint> kpt_matches_a, kpt_matches_b;
  std::vector<cv::Point2f> pt_matches_a;
  std::vector<cv::Point2f> pt_matches_b;
  cv::Mat inlier_mask, homography;
  std::vector<cv::KeyPoint> inliers_a, inliers_b;
  std::vector<cv::DMatch> inlier_matches;

  matcher.knnMatch(keyframe_a->orb_descriptors_, keyframe_b->orb_descriptors_, matches, 2);
  for (unsigned i = 0; i < matches.size(); i++) {
    if (matches[i][0].distance < nn_matching_ratio_ * matches[i][1].distance) {
      pt_matches_a.push_back(keyframe_a->fast_kpts_undist_[matches[i][0].queryIdx].pt);
      pt_matches_b.push_back(keyframe_b->fast_kpts_undist_[matches[i][0].trainIdx].pt);
      kpt_matches_a.push_back(keyframe_a->fast_kpts_undist_[matches[i][0].queryIdx]);
      kpt_matches_b.push_back(keyframe_b->fast_kpts_undist_[matches[i][0].trainIdx]);
    }
  }

  num_matches = matches.size();
  num_valid_matches = pt_matches_a.size();
  matches_ratio = (double)num_matches / (double)num_valid_matches;

  if (matches_ratio < 0.3 || num_valid_matches < 4) {
    std::cout << "Matches ratio below threshold. Value = " << matches_ratio << std::endl;
    return 0;
  }

  // std::cout << "Matches ratio over threshold" << std::endl;
  // std::cout << "  Homography estimation" << std::endl;
  homography = cv::findHomography(pt_matches_a, pt_matches_b, cv::RANSAC, ransac_thresh_, inlier_mask);

  if (num_valid_matches >= 4 && !homography.empty()) {
    // std::cout << "More than 4 matches, valid homography" << std::endl;
    for (size_t i = 0; i < num_valid_matches; i++) {
      if (inlier_mask.at<uchar>(i)) {
        int new_i = static_cast<int>(inliers_a.size());
        inliers_a.push_back(kpt_matches_a[i]);
        inliers_b.push_back(kpt_matches_b[i]);
        inlier_matches.push_back(cv::DMatch(new_i, new_i, 0));
      }
    }
    num_inliers = inliers_a.size();
    inliers_ratio = (double)num_inliers * (1.0 / (double)num_valid_matches);

    // if (inliers_ratio > 0.1) {
    //   std::cout << "  number of matches = " << num_valid_matches << std::endl;
    //   std::cout << "  num inliers " << num_inliers << std::endl;
    //   std::cout << "  inliers ratio " << inliers_ratio << std::endl;
    //   std::cout << "  matches ratio " << matches_ratio << std::endl;
    // }

    if (inliers_ratio > inliers_ratio_thresh_) {
      std::cout << "Visual loop match" << std::endl;
      // std::cout << "  number of matches = " << num_valid_matches << std::endl;
      // std::cout << "  num inliers " << num_inliers << std::endl;
      // std::cout << "  inliers ratio " << inliers_ratio << std::endl;
      // std::cout << "  matches ratio " << matches_ratio << std::endl;
    }

    return inliers_ratio;
  }

  return 0;
}

std::vector<Loop::Ptr> VisualLoopDetector::detect(
    const std::vector<Keyframe::Ptr>& keyframes, const std::vector<Keyframe::Ptr>& new_keyframes) {
  if (keyframe_db_->getNumberKeyframes() < 2 || new_loops_.size() > 0) {
    return new_loops_;
  }
  const auto& ref_keyframe = keyframe_db_->getLatestKeyframe();

  if (!ref_keyframe->has_img_) {
    return new_loops_;
  }

  const DBoW2::BowVector& ref_vBoW_vec = ref_keyframe->vBoW_vec_;
  auto candidates = findCloseCandidates(keyframes, ref_keyframe);

  float min_bow_score = 1;
  float max_bow_score = -1;
  int max_id = -1;
  const size_t num_candidates = candidates.size();
  Keyframe::Ptr best_match;
  for (size_t i = 0; i < num_candidates; i++) {
    Keyframe::Ptr curr_candidate = candidates[i];
    if (i == (size_t)ref_keyframe->node_->id() || !curr_candidate->has_img_) {
      continue;
    }

    const DBoW2::BowVector curr_vBoW_vec = curr_candidate->vBoW_vec_;
    float bow_score = orb_vocabulary_->score(ref_vBoW_vec, curr_vBoW_vec);

    if (bow_score < min_bow_score) {
      min_bow_score = bow_score;
    }

    if (bow_score > max_bow_score) {
      max_bow_score = bow_score;
      max_id = curr_candidate->node_->id();
      best_match = curr_candidate;
    }
  }

  if (max_bow_score > 0) {
    double ratio = getInliersRatio(best_match, ref_keyframe);
    if (ratio > inliers_ratio_thresh_) {
      // std::cout << "\033[1;36m##########################" << std::endl;
      // std::cout << "\033[1;36m  BoW min score = " << min_bow_score << std::endl;
      // std::cout << "\033[1;36m  BoW max score = " << max_bow_score << std::endl;
      // std::cout << "\033[1;36m  Ref keyframe id = " << ref_keyframe->node_->id() << std::endl;
      // std::cout << "\033[1;36m  Matched keyframe id = " << best_match->node_->id() << std::endl;

      pcl::PointCloud<PointI>::Ptr aligned_cloud(new pcl::PointCloud<PointI>());
      pcl::PointCloud<PointI>::Ptr ref_cloud(new pcl::PointCloud<PointI>());
      pcl::PointCloud<PointI>::Ptr candidate_cloud(new pcl::PointCloud<PointI>());
      pcl::copyPointCloud(*ref_keyframe->cloud_, *ref_cloud);
      pcl::copyPointCloud(*best_match->cloud_, *candidate_cloud);
      transformCloudToFrame(ref_cloud, ref_keyframe->node_->estimate(), map_frame_id_);
      transformCloudToFrame(candidate_cloud, best_match->node_->estimate(), map_frame_id_);
      Eigen::Matrix4f guess = (ref_keyframe->node_->estimate().inverse() * best_match->node_->estimate()).matrix().cast<float>();

      Eigen::Matrix4f relative_pose;
      if (estimateRigidTransform(candidate_cloud, ref_cloud, aligned_cloud, guess, relative_pose)) {
        last_edge_accum_distance_ = ref_keyframe->accum_distance_;
        std::cout << "\033[1;32m";
        std::cout << "  Visual detector - Loop found!!" << std::endl;
        std::cout << "    Inliers ratio = " << ratio << std::endl;
        std::cout << "\033[0m";

        Loop::Ptr new_loop = std::make_shared<Loop>(keyframes[max_id], ref_keyframe, relative_pose.inverse(), detector_type_, inf_matrix_);

        // candidate_loops_.push_back(new_loop);
        new_loops_.push_back(new_loop);

        // std::cout << relative_pose << std::endl;
        if (publish_aligned_clouds_)
          publishClouds(candidate_cloud, ref_cloud, aligned_cloud);

        std::lock_guard<std::mutex> lock(cv_window_mutex_);
        matched_keyframes_.first = ref_keyframe;
        matched_keyframes_.second = best_match;
      }
    }
  } else {
    std::lock_guard<std::mutex> lock(cv_window_mutex_);
    matched_keyframes_.first = nullptr;
    matched_keyframes_.second = nullptr;
  }

  return new_loops_;
}

void VisualLoopDetector::visualizeImagesThread() {
  // Create external window to display image
  // cv::namedWindow("Matched keyframes", cv::WINDOW_GUI_NORMAL);

  // Image size for displaying in rviz
  cv::Size size(camera_->img_width_ / 2, camera_->img_height_ / 2);

  while (ros::ok()) {
    if (!keyframe_db_->getNumberKeyframes()) {
      continue;
    }

    cv_window_mutex_.lock();
    Keyframe::Ptr last_keyframe = keyframe_db_->getLatestKeyframe();
    Keyframe::Ptr matched_keyframe_a = matched_keyframes_.first;
    Keyframe::Ptr matched_keyframe_b = matched_keyframes_.second;
    cv_window_mutex_.unlock();

    if (!last_keyframe->has_img_ || !new_image_) {
      continue;
    }

    cv::Mat curr_img, match_img;

    // Draw current keyframe.
    cv::drawKeypoints(last_keyframe->img_, last_keyframe->fast_kpts_undist_, curr_img, cv::Scalar(0, 255, 0));
    cv::resize(curr_img, curr_img, size);
    cv::putText(
        curr_img, std::to_string(last_keyframe->node_->id()), cv::Point(size.width / 2, size.height / 2), cv::FONT_HERSHEY_DUPLEX, 5,
        cv::Scalar(200, 100, 150), 2, CV_AA);

    if (matched_keyframe_a && matched_keyframe_b) {
      cv::Mat keyframe_a_img, keyframe_b_img;
      cv::drawKeypoints(matched_keyframe_a->img_, matched_keyframe_a->fast_kpts_undist_, keyframe_a_img, cv::Scalar(0, 255, 0));
      cv::resize(keyframe_a_img, keyframe_a_img, size);
      cv::putText(
          keyframe_a_img, std::to_string(matched_keyframe_a->node_->id()), cv::Point(size.width / 2, size.height / 2),
          cv::FONT_HERSHEY_DUPLEX, 5, cv::Scalar(200, 100, 150), 2, CV_AA);

      cv::drawKeypoints(matched_keyframe_b->img_, matched_keyframe_b->fast_kpts_undist_, keyframe_b_img, cv::Scalar(0, 255, 0));
      cv::resize(keyframe_b_img, keyframe_b_img, size);
      cv::putText(
          keyframe_b_img, std::to_string(matched_keyframe_b->node_->id()), cv::Point(size.width / 2, size.height / 2),
          cv::FONT_HERSHEY_DUPLEX, 5, cv::Scalar(200, 100, 150), 2, CV_AA);

      cv::vconcat(keyframe_a_img, keyframe_b_img, match_img);
    } else {
      match_img = curr_img.clone() * 0;
    }

    // Publish images.
    sensor_msgs::ImagePtr curr_img_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", curr_img).toImageMsg();
    current_kf_pub_.publish(curr_img_msg);

    sensor_msgs::ImagePtr match_img_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", match_img).toImageMsg();
    matched_kfs_pub_.publish(match_img_msg);

    new_image_ = false;
  }
}

}  // namespace vl_slam
