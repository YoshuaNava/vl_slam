
#include "vl_slam/segmatch_loop_detector.hpp"

#include <iostream>

#include <g2o/types/slam3d/vertex_se3.h>

#include <pcl/filters/crop_box.h>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

namespace vl_slam {

SegMatchLoopDetector::SegMatchLoopDetector(ros::NodeHandle& nh, KeyframeDatabase::Ptr& keyframe_db)
    : inf_matrix_(Eigen::MatrixXd::Identity(6, 6)),
      pose_at_last_localization_set_(false),
      dynamic_ground_removal_(false),
      trajectory_updated_(true) {
  const std::string ns = "vl_slam/";
  const std::string module_name = "SegMatch/";

  // Robot frames
  map_frame_id_ = nh.param<std::string>(ns + "/map_frame_id", "map");
  odom_frame_id_ = nh.param<std::string>(ns + "/odom_frame_id", "odom");
  robot_frame_id_ = nh.param<std::string>(ns + "/robot_frame_id", "camera");

  ground_seg_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(module_name + "ground_seg_cloud", 1, true);

  local_map_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(module_name + "local_map_cloud", 1, true);

  // Load information matrix
  std::vector<float> inf_matrix_vector;
  nh.getParam(ns + module_name + "information_matrix", inf_matrix_vector);
  for (size_t i = 0; i < inf_matrix_vector.size(); i++)
    inf_matrix_(i, i) = inf_matrix_vector[i];

  // SegMatchWorker parameters.
  segmatch_worker_params_ = segmatch_ros::getSegMatchWorkerParams(nh, ns);
  segmatch_worker_params_.world_frame = map_frame_id_;
  std::unique_ptr<segmatch::NormalEstimator> normal_estimator = nullptr;
  const std::string& segmenter_type = segmatch_worker_params_.segmatch_params.segmenter_params.segmenter_type;
  const bool needs_normal_estimation =
      (segmenter_type == "SimpleSmoothnessConstraints") || (segmenter_type == "IncrementalSmoothnessConstraints");

  if (needs_normal_estimation) {
    normal_estimator = segmatch::NormalEstimator::create(
        segmatch_worker_params_.segmatch_params.normal_estimator_type,
        segmatch_worker_params_.segmatch_params.radius_for_normal_estimation_m);
  }

  local_maps_.emplace_back(segmatch_worker_params_.segmatch_params.local_map_params, std::move(normal_estimator));

  if (segmatch_worker_params_.localize || segmatch_worker_params_.close_loops) {
    segmatch_worker_.init(nh, segmatch_worker_params_, 1);
  }

  nh.param(ns + "/SegMatchWorker/clear_local_map_after_loop_closure", clear_local_map_after_loop_closure_, false);

  nh.param(ns + "/SegMatchWorker/do_icp_step_on_loop_closures", do_icp_step_on_loop_closures_, false);

  icp_fitness_thresh_ = nh.param<double>(ns + module_name + "icp_fitness_thresh", 20.0);
  if (do_icp_step_on_loop_closures_)
    registration_ = select_registration_method(nh);

  LOG(INFO) << "Segmatch initialized!" << std::endl;

  // Ground segmentation params
  nh.param(ns + "/ground_segmentation/dynamic_ground_removal", dynamic_ground_removal_, false);
  if (dynamic_ground_removal_)
    LOG(INFO) << "Dynamic ground removal";
  else
    LOG(INFO) << "Static ground removal set";
  LOG(INFO) << std::endl;

  nh.param(ns + "/ground_segmentation/min_z_ground", min_z_ground_, -1.0);

  nh.param(ns + "/ground_segmentation/visualize", ground_seg_params_.visualize, ground_seg_params_.visualize);
  nh.param(ns + "/ground_segmentation/n_bins", ground_seg_params_.n_bins, ground_seg_params_.n_bins);
  nh.param(ns + "/ground_segmentation/n_segments", ground_seg_params_.n_segments, ground_seg_params_.n_segments);
  nh.param(ns + "/ground_segmentation/max_dist_to_line", ground_seg_params_.max_dist_to_line, ground_seg_params_.max_dist_to_line);
  nh.param(ns + "/ground_segmentation/max_slope", ground_seg_params_.max_slope, ground_seg_params_.max_slope);
  nh.param(ns + "/ground_segmentation/long_threshold", ground_seg_params_.long_threshold, ground_seg_params_.long_threshold);
  nh.param(ns + "/ground_segmentation/max_long_height", ground_seg_params_.max_long_height, ground_seg_params_.max_long_height);
  nh.param(ns + "/ground_segmentation/max_start_height", ground_seg_params_.max_start_height, ground_seg_params_.max_start_height);
  nh.param(ns + "/ground_segmentation/sensor_height", ground_seg_params_.sensor_height, ground_seg_params_.sensor_height);
  nh.param(ns + "/ground_segmentation/line_search_angle", ground_seg_params_.line_search_angle, ground_seg_params_.line_search_angle);
  nh.param(ns + "/ground_segmentation/n_threads", ground_seg_params_.n_threads, ground_seg_params_.n_threads);

  // Params that need to be squared.
  double r_min, r_max, max_fit_error;
  if (nh.getParam(ns + "/ground_segmentation/r_min", r_min)) {
    ground_seg_params_.r_min_square = r_min * r_min;
  }
  if (nh.getParam(ns + "/ground_segmentation/r_max", r_max)) {
    ground_seg_params_.r_max_square = r_max * r_max;
  }
  if (nh.getParam(ns + "/ground_segmentation/max_fit_error", max_fit_error)) {
    ground_seg_params_.max_error_square = max_fit_error * max_fit_error;
  }
  LOG(INFO) << "Ground segmenter params loaded!" << std::endl;

  keyframe_db_ = keyframe_db;
  detection_thread_started_ = false;

  // Debugging - Source/Target/Aligned clouds
  publish_aligned_clouds_ = nh.param<bool>(ns + "/SegMatchWorker/publish_aligned_clouds", false);
  if (publish_aligned_clouds_) {
    source_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "source_cloud", 1, true);
    target_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "target_cloud", 1, true);
    aligned_cloud_pub_ = nh.advertise<sensor_msgs::PointCloud2>(ns + module_name + "aligned_cloud", 1, true);
  }
}

void SegMatchLoopDetector::startThread() {
  segmatch_thread_ = std::thread(&SegMatchLoopDetector::segMatchThread, this);
  detection_thread_started_ = true;
}

void SegMatchLoopDetector::staticGroundRemoval(
    const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>& ground_seg_cloud) const {
  // "Static" = Box filter ground removal
  const double pos_pseudo_inf = 10000000000.0;
  const double neg_pseudo_inf = -1.0 * pos_pseudo_inf;
  pcl::CropBox<pcl::PointXYZ> boxFilter;
  boxFilter.setMin(Eigen::Vector4f(neg_pseudo_inf, neg_pseudo_inf, min_z_ground_, 1.0));
  boxFilter.setMax(Eigen::Vector4f(pos_pseudo_inf, pos_pseudo_inf, pos_pseudo_inf, 1.0));
  boxFilter.setInputCloud(cloud);
  boxFilter.filter(ground_seg_cloud);
}

void SegMatchLoopDetector::dynamicGroundRemoval(
    const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>& ground_seg_cloud) const {
  // "Dynamic" = Ground segmentation using https://github.com/lorenwel/linefit_ground_segmentation
  LOG(INFO) << "Dynamic" << std::endl;
  GroundSegmentation segmenter(ground_seg_params_);
  std::vector<int> labels;
  segmenter.segment(*cloud, &labels);

  ground_seg_cloud.header = cloud->header;
  ground_seg_cloud.header.frame_id = robot_frame_id_;
  for (size_t i = 0; i < cloud->size(); ++i) {
    if (labels[i] != 1)
      ground_seg_cloud.push_back(cloud->points[i]);
  }
  ground_seg_cloud.header.frame_id = map_frame_id_;
}

void SegMatchLoopDetector::removeGround(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud) const {
  pcl::PointCloud<pcl::PointXYZ> ground_seg_cloud;
  if (dynamic_ground_removal_) {
    dynamicGroundRemoval(cloud, ground_seg_cloud);
  } else {
    staticGroundRemoval(cloud, ground_seg_cloud);
  }

  // Ground-segmented cloud transformation to map frame
  const Eigen::Isometry3d T_odom2map = keyframe_db_->getTransformOdomToMap();
  const Eigen::Isometry3d pose_map = T_odom2map * keyframe_db_->getLatestKeyframe()->T_odom_;

  pcl::transformPointCloud(ground_seg_cloud, ground_seg_cloud, pose_map.matrix());
  pcl::copyPointCloud(ground_seg_cloud, *cloud);

  if (ground_seg_params_.visualize)
    publishPointCloud(ground_seg_cloud, map_frame_id_, ros::Time::now(), &ground_seg_cloud_pub_);
}

void SegMatchLoopDetector::processNewKeyframe(const Keyframe::Ptr keyframe) {
  if (!detection_thread_started_) {
    return;
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::copyPointCloud(*keyframe->cloud_, *cloud);

  removeGround(cloud);

  local_map_queue_.push_back(*cloud);
}

void SegMatchLoopDetector::publishClouds(
    const pcl::PointCloud<PointI>::Ptr& source, const pcl::PointCloud<PointI>::Ptr& target, const pcl::PointCloud<PointI>::Ptr& aligned) {
  publishPointCloud(source, map_frame_id_, ros::Time::now(), &source_cloud_pub_);
  publishPointCloud(target, map_frame_id_, ros::Time::now(), &target_cloud_pub_);
  publishPointCloud(aligned, map_frame_id_, ros::Time::now(), &aligned_cloud_pub_);
}

bool SegMatchLoopDetector::performIcpAfterLoopClosure(
    const Keyframe::Ptr keyframe_a, const Keyframe::Ptr keyframe_b, const Eigen::Matrix4f guess, Eigen::Matrix4f& loop_transform) {
  pcl::PointCloud<PointI>::Ptr aligned(new pcl::PointCloud<PointI>());
  pcl::PointCloud<PointI>::Ptr ref_cloud(new pcl::PointCloud<PointI>());
  pcl::PointCloud<PointI>::Ptr candidate_cloud(new pcl::PointCloud<PointI>());

  pcl::copyPointCloud(*keyframe_b->cloud_, *ref_cloud);
  transformCloudToFrame(ref_cloud, keyframe_b->node_->estimate(), map_frame_id_);
  pcl::copyPointCloud(*keyframe_a->cloud_, *candidate_cloud);
  transformCloudToFrame(candidate_cloud, keyframe_a->node_->estimate(), map_frame_id_);

  registration_->setInputTarget(ref_cloud);
  registration_->setInputSource(candidate_cloud);
  registration_->align(*aligned);
  double score = registration_->getFitnessScore();
  publishClouds(candidate_cloud, ref_cloud, aligned);

  if (!registration_->hasConverged() || score > icp_fitness_thresh_) {
    return false;
  }

  loop_transform = registration_->getFinalTransformation();

  return true;
}

void SegMatchLoopDetector::segMatchThread() {
  // Terminate the thread if localization and loop closure are not needed.
  if (!segmatch_worker_params_.localize && !segmatch_worker_params_.close_loops)
    return;

  size_t track_id = 0;
  // Number of tracks skipped because waiting for new voxels to activate.
  unsigned int skipped_tracks_count = 0u;
  ros::Duration sleep_duration(kSegMatchSleepTime_s);

  unsigned int n_loops = 0u;

  while (ros::ok()) {
    if (!keyframe_db_->getNumberKeyframes() || !trajectory_updated_)
      continue;

    if (skipped_tracks_count == 1) {
      skipped_tracks_count = 0u;
      sleep_duration.sleep();
    }

    Keyframe::Ptr last_keyframe = keyframe_db_->getLatestKeyframe();
    std::vector<PointCloud> new_points;
    new_points.swap(local_map_queue_);

    // Get the queued points.
    if (new_points.empty()) {
      ++skipped_tracks_count;
      segmatch_worker_.publish();
      continue;
    } else {
      if (!first_points_received_) {
        first_points_received_ = true;
      }
    }

    // Update the local map with the new points and the new pose.
    double time_ns = keyframe_db_->rosTimeToCurveTime(last_keyframe->stamp_.toNSec());
    laser_slam::Pose current_pose = convertEigenToPose(time_ns, last_keyframe->node_->estimate());
    local_maps_[track_id].updatePoseAndAddPoints(new_points, current_pose);

    // // Process the source cloud.
    if (segmatch_worker_params_.localize) {
      if (segmatch_worker_.processLocalMap(local_maps_[track_id], current_pose, track_id)) {
        if (!pose_at_last_localization_set_) {
          pose_at_last_localization_set_ = true;
          pose_at_last_localization_ = current_pose.T_w;
        } else {
          pose_at_last_localization_ = current_pose.T_w;
        }
      }
    } else {
      laser_slam::RelativePose loop_closure;

      // If there is a loop closure.
      if (segmatch_worker_.processLocalMap(local_maps_[track_id], current_pose, track_id, &loop_closure)) {
        LOG(INFO) << "\033[1;32m SegMatch - Found loop closure! track_id_a: " << loop_closure.track_id_a
                  << " time_a_ns: " << loop_closure.time_a_ns << " key_a: " << loop_closure.key_a
                  << " track_id_b: " << loop_closure.track_id_b << " time_b_ns: " << loop_closure.time_b_ns
                  << " key_a: " << loop_closure.key_b << "\033[0m";

        segmatch_worker_.publish();

        if (clear_local_map_after_loop_closure_)
          local_maps_[track_id].clear();

        // Find the keyframes between which there is a loop
        Keyframe::Ptr keyframe_a, keyframe_b;
        keyframe_db_->findKeyframeByTimestamp(loop_closure.time_a_ns, keyframe_a);
        keyframe_db_->findKeyframeByTimestamp(loop_closure.time_b_ns, keyframe_b);

        // Build the rigid transformation
        laser_slam::SE3 w_T_a_b = loop_closure.T_a_b;

        Eigen::Matrix4f loop_transform;

        if (do_icp_step_on_loop_closures_) {
          if (performIcpAfterLoopClosure(keyframe_a, keyframe_b, w_T_a_b.getTransformationMatrix().cast<float>(), loop_transform)) {
            LOG(INFO) << "\033[1;32m SegMatch - Loop verified with extra ICP"
                      << "\033[0m";
          } else {
            LOG(INFO) << "\033[1;31m SegMatch - Loop rejected"
                      << "\033[0m";
            continue;
          }
        } else {
          loop_transform = w_T_a_b.getTransformationMatrix().cast<float>();
        }

        // std::cout << "Loop transform guess = " << w_T_a_b.getTransformationMatrix() << std::endl;
        // std::cout << "Loop transform final = " << loop_transform << std::endl;

        Loop::Ptr new_loop = std::make_shared<Loop>(keyframe_a, keyframe_b, loop_transform, detector_type_, inf_matrix_);
        new_loops_.push_back(new_loop);
        detected_loops_.push_back(new_loop);

        trajectory_updated_ = false;
      }

      n_loops++;
      // LOG(INFO) << "SegMatch thread iteration number " << n_loops << ".";
    }

    segmatch::MapCloud local_maps;
    local_maps = local_maps_[track_id].getFilteredPoints();
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::copyPointCloud(local_maps, cloud);
    publishPointCloud(cloud, map_frame_id_, ros::Time::now(), &local_map_cloud_pub_);

    skipped_tracks_count = 0;
  }
}

void SegMatchLoopDetector::updateTrajectory() {
  laser_slam::Trajectory trajectory;
  const size_t num_keyframes = keyframe_db_->getNumberKeyframes();
  for (size_t i = 0; i < num_keyframes; i++) {
    double time_pose = keyframe_db_->rosTimeToCurveTime(keyframe_db_->getKeyframe(i)->stamp_.toNSec());
    Eigen::Isometry3d T = keyframe_db_->getKeyframe(i)->node_->estimate();
    laser_slam::SE3 T_w = convertEigenToSE3(T);
    trajectory.emplace(time_pose, T_w);
  }
  segmatch_worker_.update(trajectory);

  trajectory_updated_ = true;
}

}  // namespace vl_slam
