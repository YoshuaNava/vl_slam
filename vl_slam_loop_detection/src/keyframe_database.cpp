
#include "vl_slam/keyframe_database.hpp"

#include <mutex>

#include <Eigen/Dense>

#include <g2o/types/slam3d/vertex_se3.h>

#include <ros/ros.h>

namespace vl_slam {

KeyframeDatabase::KeyframeDatabase(ros::NodeHandle& nh)
    : is_first_(true), T_odom2map_(Eigen::Matrix4f::Identity()), prev_keypose_(Eigen::Isometry3d::Identity()) {
  // The minimum tranlational distance and rotation angle between keyframes.
  // If this value is zero, frames are always compared with the previous frame
  keyframe_delta_trans_ = nh.param<double>("/vl_slam/keyframe_delta_trans", 1.5);
  keyframe_delta_angle_ = nh.param<double>("/vl_slam/keyframe_delta_angle", 0.5);

  keyframe_store_clouds_ = nh.param<bool>("/vl_slam/keyframe_store_clouds", true);

  accum_distance_ = 0.0;
}

bool KeyframeDatabase::thresholdNewPose(const Eigen::Isometry3d& pose) {
  // First frame is always registered to the graph
  if (is_first_) {
    is_first_ = false;
    prev_keypose_ = pose;
    return true;
  }

  // Calculate the delta transformation from the previous keyframe
  Eigen::Isometry3d delta = prev_keypose_.inverse() * pose;
  double dx = delta.translation().norm();
  double da = std::acos(Eigen::Quaterniond(delta.linear()).w());

  // Threshold deltas
  if (dx < keyframe_delta_trans_ && da < keyframe_delta_angle_) {
    return false;
  }

  accum_distance_ += dx;
  prev_keypose_ = pose;
  return true;
}

double KeyframeDatabase::getAccumDistance() const {
  return accum_distance_;
}

Eigen::Isometry3d KeyframeDatabase::getTransformOdomToMap() {
  std::lock_guard<std::mutex> lock(T_odom2map_mutex_);
  Eigen::Isometry3d odom2map(T_odom2map_.cast<double>());

  return odom2map;
}

Eigen::Isometry3d KeyframeDatabase::getLatestPose() {
  const auto& keyframe = getLatestKeyframe();
  Eigen::Isometry3d T = keyframe->node_->estimate();
  return T;
}

void KeyframeDatabase::updateOdomToMapTransform(Eigen::Isometry3d& T_odom2map, ros::Time& stamp) {
  const auto& keyframe = getLatestKeyframe();
  stamp = keyframe->stamp_;
  T_odom2map = keyframe->node_->estimate() * keyframe->T_odom_.inverse();

  std::lock_guard<std::mutex> lock(T_odom2map_mutex_);
  T_odom2map_ = T_odom2map.matrix().cast<float>();
}

void KeyframeDatabase::addNewKeyframe(const Keyframe::Ptr& keyframe) {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  if(!keyframe_store_clouds_) {
    keyframe->cloud_.reset(new pcl::PointCloud<PointI>());
    keyframe->has_cloud_ = false;
  }

  keyframes_.push_back(keyframe);
  new_keyframes_.push_back(keyframe);
}

void KeyframeDatabase::addNewKeyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, const pcl::PointCloud<PointI>::Ptr& cloud) {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  const double accum_dist = getAccumDistance();
  Keyframe::Ptr keyframe(new Keyframe(stamp, T_odom, accum_dist, cloud));
  addNewKeyframe(keyframe);
}

std::vector<Keyframe::Ptr>& KeyframeDatabase::getNewKeyframes() {
  return new_keyframes_;
}

std::vector<Keyframe::Ptr> KeyframeDatabase::getNewKeyframesCopy() {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  std::vector<Keyframe::Ptr> snapshot(new_keyframes_.size());
  std::transform(new_keyframes_.begin(), new_keyframes_.end(), snapshot.begin(), [](const Keyframe::Ptr& k) { return k; });

  return snapshot;
}

bool KeyframeDatabase::clearNewKeyframes() {
  new_keyframes_.clear();
  return true;
}

size_t KeyframeDatabase::getNumberKeyframes() {
  return keyframes_.size();
}

std::vector<Keyframe::Ptr> KeyframeDatabase::getKeyframeList() {
  std::lock_guard<std::mutex> lock(loops_mutex_);
  return keyframes_;
}

std::vector<Keyframe::Ptr>& KeyframeDatabase::getKeyframes() {
  return keyframes_;
}

std::vector<Keyframe::Ptr> KeyframeDatabase::getKeyframesCopy() {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  std::vector<Keyframe::Ptr> snapshot(keyframes_.size());
  std::transform(keyframes_.begin(), keyframes_.end(), snapshot.begin(), [](const Keyframe::Ptr& k) { return k; });

  return snapshot;
}

std::vector<KeyframeSnapshot::Ptr> KeyframeDatabase::takeKeyframesSnapshot() {
  std::vector<KeyframeSnapshot::Ptr> snapshot(keyframes_.size());
  std::transform(keyframes_.begin(), keyframes_.end(), snapshot.begin(), [=](const Keyframe::Ptr& k) {
    return std::make_shared<KeyframeSnapshot>(k);
  });

  return snapshot;
}

Keyframe::Ptr KeyframeDatabase::getKeyframe(size_t idx) {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  return keyframes_[idx];
}

Keyframe::Ptr KeyframeDatabase::getLatestKeyframe() {
  std::lock_guard<std::mutex> lock(keyframes_mutex_);
  return keyframes_.back();
}

size_t KeyframeDatabase::getNumberLoops() {
  std::lock_guard<std::mutex> lock(loops_mutex_);
  return loops_.size();
}

std::vector<Loop::Ptr> KeyframeDatabase::getLoopList() {
  std::lock_guard<std::mutex> lock(loops_mutex_);
  return loops_;
}

Loop::Ptr KeyframeDatabase::getLoop(size_t idx) {
  std::lock_guard<std::mutex> lock(loops_mutex_);
  return loops_[idx];
}

void KeyframeDatabase::addNewLoops(const std::vector<Loop::Ptr> new_loops) {
  std::copy(new_loops.begin(), new_loops.end(), std::back_inserter(loops_));
}

double KeyframeDatabase::rosTimeToCurveTime(const double& timestamp_ns) {
  if (!base_time_set_) {
    base_time_ns_ = timestamp_ns;
    base_time_set_ = true;
  }
  return timestamp_ns - base_time_ns_;
}

double KeyframeDatabase::curveTimeToRosTime(const double& timestamp_ns) const {
  return timestamp_ns + base_time_ns_;
}

bool KeyframeDatabase::findKeyframeByTimestamp(const double& timestamp_ns, Keyframe::Ptr& kf) {
  bool found = false;
  std::vector<Keyframe::Ptr>::const_iterator it = keyframes_.begin();
  while (it != keyframes_.end() && !found) {
    double curr_time = rosTimeToCurveTime((*it)->stamp_.toNSec());
    if (curr_time == timestamp_ns) {
      kf = (*it);
      found = true;
    } else {
      ++it;
    }
  }
  return found;
}

}  // namespace vl_slam