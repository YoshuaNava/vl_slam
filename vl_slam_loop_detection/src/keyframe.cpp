#include "vl_slam/keyframe.hpp"

#include <boost/filesystem.hpp>

#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>

#include <DBoW2.h>
#include <DUtils/DUtils.h>
#include <DVision/DVision.h>

#include <g2o/types/slam3d/vertex_se3.h>

namespace vl_slam {

bool Keyframe::initial_run = true;
float Keyframe::img_min_x_, Keyframe::img_max_x_, Keyframe::img_min_y_, Keyframe::img_max_y_;
float Keyframe::grid_element_width_inv_, Keyframe::grid_element_height_inv_;

Keyframe::Keyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, double accum_distance)
    : stamp_(stamp), T_odom_(T_odom), accum_distance_(accum_distance), node_(nullptr), has_cloud_(false), has_img_(false) {}

Keyframe::Keyframe(
    const ros::Time& stamp, const Eigen::Isometry3d& T_odom, double accum_distance, const pcl::PointCloud<PointI>::Ptr& cloud)
    : stamp_(stamp), T_odom_(T_odom), accum_distance_(accum_distance), node_(nullptr), cloud_(cloud), has_cloud_(true), has_img_(false) {}

bool Keyframe::processImage(
    const cv::Mat& img, const std::shared_ptr<OrbVocabulary>& orb_vocabulary, const std::shared_ptr<OrbExtractor>& orb_extractor,
    const PinholeCamera::Ptr& camera) {
  camera_ = camera;
  img_ = img.clone();
  has_img_ = true;

  // Scale Level Info
  orb_vocabulary_ = orb_vocabulary;
  scale_levels_ = orb_extractor->GetLevels();
  scale_factor_ = orb_extractor->GetScaleFactor();
  log_scale_factor_ = log(scale_factor_);
  scale_factors_ = orb_extractor->GetScaleFactors();
  inv_scale_factors_ = orb_extractor->GetInverseScaleFactors();
  level_sigma2_ = orb_extractor->GetScaleSigmaSquares();
  inv_level_sigma2_ = orb_extractor->GetInverseScaleSigmaSquares();

  // ORB extraction
  (*orb_extractor)(img, cv::Mat(), fast_kpts_, orb_descriptors_);

  num_features_ = fast_kpts_.size();

  if (num_features_ == 0)
    return false;

  undistortKeyPoints();

  // This is done only for the first Frame (or after a change in the calibration)
  if (initial_run) {
    computeImageBounds(img);

    grid_element_width_inv_ = static_cast<float>(FRAME_GRID_COLS) / static_cast<float>(img_max_x_ - img_min_x_);
    grid_element_height_inv_ = static_cast<float>(FRAME_GRID_ROWS) / static_cast<float>(img_max_y_ - img_min_y_);

    initial_run = false;
  }

  assignFeaturesToGrid();

  computeBoW();

  return true;
}

void Keyframe::undistortKeyPoints() {
  if (camera_->dist_coeffs_.at<float>(0) == 0.0) {
    fast_kpts_undist_ = fast_kpts_;
    return;
  }

  // Fill matrix with points
  cv::Mat mat(num_features_, 2, CV_32F);
  for (int i = 0; i < num_features_; i++) {
    mat.at<float>(i, 0) = fast_kpts_[i].pt.x;
    mat.at<float>(i, 1) = fast_kpts_[i].pt.y;
  }

  // Undistort points
  mat = mat.reshape(2);
  K_ = camera_->K_.clone();
  cv::undistortPoints(mat, mat, camera_->K_, camera_->dist_coeffs_, cv::Mat(), K_);
  mat = mat.reshape(1);

  // Fill undistorted keypoint vector
  fast_kpts_undist_.resize(num_features_);
  for (int i = 0; i < num_features_; i++) {
    cv::KeyPoint kp = fast_kpts_[i];
    kp.pt.x = mat.at<float>(i, 0);
    kp.pt.y = mat.at<float>(i, 1);
    fast_kpts_undist_[i] = kp;
  }
}

void Keyframe::computeImageBounds(const cv::Mat& imLeft) {
  if (camera_->dist_coeffs_.at<float>(0) != 0.0) {
    cv::Mat mat(4, 2, CV_32F);
    mat.at<float>(0, 0) = 0.0;
    mat.at<float>(0, 1) = 0.0;
    mat.at<float>(1, 0) = camera_->img_width_;
    mat.at<float>(1, 1) = 0.0;
    mat.at<float>(2, 0) = 0.0;
    mat.at<float>(2, 1) = camera_->img_height_;
    mat.at<float>(3, 0) = camera_->img_width_;
    mat.at<float>(3, 1) = camera_->img_height_;

    // Undistort corners
    mat = mat.reshape(2);
    cv::undistortPoints(mat, mat, camera_->K_, camera_->dist_coeffs_, cv::Mat(), K_);
    mat = mat.reshape(1);

    img_min_x_ = std::min(mat.at<float>(0, 0), mat.at<float>(2, 0));
    img_max_x_ = std::max(mat.at<float>(1, 0), mat.at<float>(3, 0));
    img_min_y_ = std::min(mat.at<float>(0, 1), mat.at<float>(1, 1));
    img_max_y_ = std::max(mat.at<float>(2, 1), mat.at<float>(3, 1));
  } else {
    img_min_x_ = 0.0f;
    img_max_x_ = img_.cols;
    img_min_y_ = 0.0f;
    img_max_y_ = img_.rows;
  }
}

bool Keyframe::posInGrid(const cv::KeyPoint& kp, int& pos_x, int& pos_y) {
  pos_x = round((kp.pt.x - img_min_x_) * grid_element_width_inv_);
  pos_y = round((kp.pt.y - img_min_y_) * grid_element_height_inv_);

  // Keypoint's coordinates are undistorted, which could cause to go out of the image
  if (pos_x < 0 || pos_x >= FRAME_GRID_COLS || pos_y < 0 || pos_y >= FRAME_GRID_ROWS)
    return false;

  return true;
}

void Keyframe::assignFeaturesToGrid() {
  int num_cells = 0.5f * (num_features_ / (FRAME_GRID_COLS * FRAME_GRID_ROWS));
  for (unsigned int i = 0; i < FRAME_GRID_COLS; i++)
    for (unsigned int j = 0; j < FRAME_GRID_ROWS; j++)
      kpts_grid_[i][j].reserve(num_cells);

  for (int i = 0; i < num_features_; i++) {
    const cv::KeyPoint& kp = fast_kpts_undist_[i];

    int grid_pos_x, grid_pos_y;
    if (posInGrid(kp, grid_pos_x, grid_pos_y))
      kpts_grid_[grid_pos_x][grid_pos_y].push_back(i);
  }
}

void Keyframe::computeBoW() {
  if (vBoW_vec_.empty() || feat_vec_.empty()) {
    std::vector<cv::Mat> descriptor_vec = toDescriptorVector(orb_descriptors_);

    // Feature vector associate features with nodes in the 4th level (from leaves up)
    // We assume the vocabulary tree has 6 levels, change the 4 otherwise
    orb_vocabulary_->transform(descriptor_vec, vBoW_vec_, feat_vec_, 4);
  }
}

std::vector<cv::Mat> Keyframe::toDescriptorVector(const cv::Mat& descriptors) {
  std::vector<cv::Mat> desc_vec;
  desc_vec.reserve(descriptors.rows);
  for (int j = 0; j < descriptors.rows; j++)
    desc_vec.push_back(descriptors.row(j));

  return desc_vec;
}

void Keyframe::dump(const std::string& directory) {
  if (!boost::filesystem::is_directory(directory)) {
    boost::filesystem::create_directory(directory);
  }

  std::ofstream ofs(directory + "/data");
  ofs << "Stamp " << stamp_.sec << " " << stamp_.nsec << "\n";

  ofs << "T_odom\n";
  ofs << T_odom_.matrix() << "\n";

  ofs << "Accum_distance " << accum_distance_ << "\n";

  if (node_) {
    ofs << "Id " << node_->id() << "\n";
  }

  pcl::io::savePCDFileBinary(directory + "/cloud.pcd", *cloud_);
}

KeyframeSnapshot::KeyframeSnapshot(const Eigen::Isometry3d& T_map, const pcl::PointCloud<PointI>::Ptr& cloud)
    : T_map_(T_map), cloud_(cloud) {}

KeyframeSnapshot::KeyframeSnapshot(const Keyframe::Ptr& keyframe) : T_map_(keyframe->node_->estimate()), cloud_(keyframe->cloud_) {}

KeyframeSnapshot::~KeyframeSnapshot() {}

}  // namespace vl_slam
