
#include "vl_slam/submap_loop_detector.hpp"

#include <iostream>

#include <sensor_msgs/PointCloud2.h>

#include <g2o/types/slam3d/vertex_se3.h>

namespace vl_slam {

SubmapLoopDetector::SubmapLoopDetector(ros::NodeHandle& nh, KeyframeDatabase::Ptr& keyframe_db) : LoopDetectorBase("submap") {
  const std::string ns = "vl_slam/";
  const std::string module_name = "submap_detector/";
  LoopDetectorBase::setup(nh, keyframe_db, module_name);

  matched_keyframes_.first = nullptr;
  matched_keyframes_.second = nullptr;
}

std::vector<Loop::Ptr> SubmapLoopDetector::detect() {
  std::vector<Keyframe::Ptr> kfs_snapshot = keyframe_db_->getKeyframesCopy();

  const auto& new_keyframe = kfs_snapshot.back();
  auto candidates = findCloseCandidates(kfs_snapshot, new_keyframe);
  auto loop = matching(candidates, new_keyframe);
  if (loop) {
    new_loops_.push_back(loop);
    detected_loops_.push_back(loop);
  }

  return new_loops_;
}

/**
 * @brief To validate a loop candidate this function applies a scan matching between keyframes consisting the loop. If they are matched
 * well, the loop keys are returned
 * @param candidate_keyframes  candidate keyframes of loop start
 * @param new_keyframe         loop end keyframe
 */
Loop::Ptr SubmapLoopDetector::matching(const std::vector<Keyframe::Ptr>& candidate_keyframes, const Keyframe::Ptr& new_keyframe) {
  if (candidate_keyframes.empty()) {
    return nullptr;
  }

  double best_score = std::numeric_limits<double>::max();
  Keyframe::Ptr best_match;
  Eigen::Matrix4f relative_pose;
  Eigen::Matrix4f best_guess;

  // std::cout << std::endl;
  // std::cout << "--- loop detection ---" << std::endl;
  // std::cout << "  Number of candidates: " << candidate_keyframes.size() << std::endl;
  // std::cout << "  Matching" << std::endl;
  const auto t1 = ros::Time::now();

  pcl::PointCloud<PointI>::Ptr aligned(new pcl::PointCloud<PointI>());
  pcl::PointCloud<PointI>::Ptr best_aligned(new pcl::PointCloud<PointI>());
  pcl::PointCloud<PointI>::Ptr ref_cloud(new pcl::PointCloud<PointI>());
  pcl::PointCloud<PointI>::Ptr candidate_cloud(new pcl::PointCloud<PointI>());

  const Eigen::Isometry3d T_key_b = new_keyframe->node_->estimate();
  pcl::copyPointCloud(*new_keyframe->cloud_, *ref_cloud);
  transformCloudToFrame(ref_cloud, new_keyframe->node_->estimate(), map_frame_id_);
  registration_->setInputTarget(ref_cloud);

  int i = 0;
  for (const auto& candidate : candidate_keyframes) {
    Eigen::Isometry3d T_key_a = candidate->node_->estimate();
    Eigen::Matrix4f guess = (T_key_b * T_key_a.inverse()).matrix().cast<float>();
    pcl::copyPointCloud(*candidate->cloud_, *candidate_cloud);
    transformCloudToFrame(candidate_cloud, candidate->node_->estimate(), map_frame_id_);

    registration_->setInputSource(candidate_cloud);
    registration_->align(*aligned);

    i++;

    double score = registration_->getFitnessScore();
    if (!registration_->hasConverged() || score > best_score) {
      continue;
    }

    best_score = score;
    best_match = candidate;
    relative_pose = registration_->getFinalTransformation();
    best_aligned = aligned;
    best_guess = guess;
    ros::Time t3 = ros::Time::now();
  }

  const ros::Time t2 = ros::Time::now();
  // std::cout << "\n  Best_score: " << boost::format("%.3f") % best_score << ",    Time: " << boost::format("%.3f") % (t2 - t1).toSec() <<
  // " [sec]" << std::endl;

  if (best_score > fitness_score_thresh_) {
    std::cout << "\033[1;31m  Submap detector - Loop not found\033[0m" << std::endl;
    return nullptr;
  }

  std::cout << "\033[1;32m  Submap detector - Loop found!!\033[0m" << std::endl;
  // std::cout << "  Relative pose: " << relative_pose.block<3, 1>(0, 3).transpose() << " \n   " <<
  // Eigen::Quaternionf(relative_pose.block<3, 3>(0, 0)).coeffs().transpose() << std::endl;

  last_edge_accum_distance_ = new_keyframe->accum_distance_;

  pcl::copyPointCloud(*best_match->cloud_, *candidate_cloud);
  transformCloudToFrame(candidate_cloud, best_match->node_->estimate(), map_frame_id_);

  if (publish_aligned_clouds_)
    publishClouds(candidate_cloud, ref_cloud, best_aligned);

  // std::cout << "Relative pose:" << std::endl << relative_pose << std::endl;
  // std::cout << "Best guess:" << std::endl << best_guess << std::endl;

  return std::make_shared<Loop>(best_match, new_keyframe, relative_pose.inverse(), detector_type_, inf_matrix_);
}

}  // namespace vl_slam
