#ifndef VL_SLAM_ROS
#define VL_SLAM_ROS

#include <ctime>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>

#include <Eigen/Dense>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <nav_msgs/Odometry.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Time.h>
#include <std_srvs/Empty.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>

#include "vl_slam/common.hpp"
#include "vl_slam/graph_slam_worker.hpp"
#include "vl_slam/keyframe.hpp"
#include "vl_slam/loop.hpp"
#include "vl_slam/map_cloud_generator.hpp"
#include "vl_slam_ros/SaveMap.h"

namespace vl_slam {

class VlSlamRos : public GraphSlamWorker {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  VlSlamRos(ros::NodeHandle& nh, ros::NodeHandle& pnh);

  // TODO doc
  void shutdown(int shutdownSignal);

 protected:
  // TODO doc
  void setup();

  // TODO doc
  void rawCloudCallback(const sensor_msgs::PointCloud2::ConstPtr& cloud_msg);

  /**
   * @brief ROS synchronized odometry & point cloud callback
   * @param odom_msg
   * @param cloud_msg
   */
  void cloudCallback(const nav_msgs::OdometryConstPtr& odom_msg, const sensor_msgs::PointCloud2::ConstPtr& cloud_msg);

  // /**
  //  * @brief ROS image callback
  //  * @param image_msg
  //  */
  void imageCallback(const sensor_msgs::Image::ConstPtr& imageData);

  // TODO doc
  void publishTfTimerCallback(const ros::TimerEvent& event);

  /**
   * @brief generate a map point cloud and publish it
   * @param event
   */
  void mapPublishTimerCallback(const ros::TimerEvent& event);

  /**
   * @brief save map data as pcd
   * @param req
   * @param res
   * @return
   */
  bool saveMapServiceCall(vl_slam_ros::SaveMapRequest& req, vl_slam_ros::SaveMapResponse& res);

  /**
   * @brief dump all data to the current directory
   * @param req
   * @param res
   * @return
   */
  bool dumpDataServiceCall(std_srvs::EmptyRequest& req, std_srvs::EmptyResponse& res);

  /**
   * @brief generate and publish array of visualization markers
   * @param event
   */
  void markersTimerCallback(const ros::TimerEvent& event);

  /**
   * @brief create visualization marker
   * @param stamp
   * @return
   */
  visualization_msgs::MarkerArray createMarkerArray(const ros::Time& stamp);

  // TODO doc
  bool logPathCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

 protected:
  // ROS
  ros::Timer tf_publish_timer_;
  ros::Timer markers_publish_timer_;
  ros::Timer map_publish_timer_;
  double tf_update_interval_;
  double markers_update_interval_;
  double map_cloud_update_interval_;

  std::vector<float> icp_nodes_markers_color_;
  std::vector<float> odom_nodes_markers_color_;
  std::vector<float> edges_markers_color_;
  double marker_scale_nodes_;
  double marker_scale_edges_;

  ros::Publisher odom2map_pub_;
  ros::Publisher input_points_pub_;
  ros::Publisher map_points_pub_;
  ros::Publisher markers_pub_;
  tf::TransformBroadcaster tf_broadcaster_;
  ros::Publisher test_points_pub_;
  ros::Publisher test_odometry_pub_;

  ros::ServiceServer dump_data_service_;
  ros::ServiceServer save_map_service_;
  ros::ServiceServer log_path_service_;

  std::unique_ptr<message_filters::Subscriber<nav_msgs::Odometry>> odom_sub_;
  std::unique_ptr<message_filters::Subscriber<sensor_msgs::PointCloud2>> cloud_sub_;
  std::unique_ptr<message_filters::TimeSynchronizer<nav_msgs::Odometry, sensor_msgs::PointCloud2>> sync_odom_cloud_;
  ros::Subscriber image_sub_;
  ros::Subscriber raw_cloud_sub_;
};

}  // namespace vl_slam

#endif