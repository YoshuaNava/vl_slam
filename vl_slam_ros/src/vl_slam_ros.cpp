
#include "vl_slam_ros.hpp"

#include <stdio.h>
#include <thread>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <cv_bridge/cv_bridge.h>
#include <opencv2/core.hpp>

#include <g2o/types/slam3d/edge_se3.h>
#include <g2o/types/slam3d/vertex_se3.h>

#include <geometry_msgs/Point.h>
#include <std_srvs/Empty.h>
#include <tf_conversions/tf_eigen.h>

#include <laser_slam/common.hpp>

#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>

#include "loam_msgs/PoseUpdate.h"
#include "loam_ros/common.h"
#include "loam_utils/common.h"
#include "ros_utils.hpp"

namespace vl_slam {

VlSlamRos::VlSlamRos(ros::NodeHandle& nh, ros::NodeHandle& pnh) : GraphSlamWorker(nh, pnh) {
  setup();
}

void VlSlamRos::shutdown(int shutdownSignal) {
  ROS_INFO("VlSlam received shutdown signal. Shutdown routine started.");
  logPath();
  ros::shutdown();
  ROS_INFO("VlSlam shutdown routine finalized.");
}

void VlSlamRos::setup() {
  // Robot frames
  map_frame_id_ = pnh_.param<std::string>("map_frame_id", "map");
  odom_frame_id_ = pnh_.param<std::string>("odom_frame_id", "odom");
  robot_frame_id_ = pnh_.param<std::string>("robot_frame_id", "camera");

  // Filtering for input clouds
  voxel_leaf_size_ = pnh_.param<double>("voxel_leaf_size", 0.1);
  apply_voxel_filter_ = pnh_.param<bool>("apply_voxel_filter", false);
  voxel_filter_.setLeafSize(voxel_leaf_size_, voxel_leaf_size_, voxel_leaf_size_);

  apply_nan_filter_ = pnh_.param<bool>("apply_nan_filter", false);

  // Full map cloud resolution
  map_cloud_resolution_ = pnh_.param<double>("map_cloud_resolution", 1.0);

  // Information matrix for new keyframes
  std::vector<float> inf_matrix_vector;
  pnh_.getParam("icp_information_matrix", inf_matrix_vector);
  for (int i = 0; i < inf_matrix_vector.size(); i++) {
    icp_inf_matrix_(i, i) = inf_matrix_vector[i];
  }

  // Visualization parameters
  marker_scale_nodes_ = pnh_.param<double>("marker_scale_nodes", 1.0);
  marker_scale_edges_ = pnh_.param<double>("marker_scale_edges", 0.1);
  if (!pnh_.getParam("icp_nodes_markers_color", icp_nodes_markers_color_)) {
    icp_nodes_markers_color_ = {0, 1.0, 0, 1.0};
  }
  if (!pnh_.getParam("odom_nodes_markers_color", odom_nodes_markers_color_)) {
    odom_nodes_markers_color_ = {0, 0.5, 0.5, 1.0};
  }
  if (!pnh_.getParam("edges_markers_color", edges_markers_color_)) {
    edges_markers_color_ = {0, 0, 1.0, 1.0};
  }

  // Path to save trajectory, timestamps and loops logs
  logging_path_ = pnh_.param<std::string>("logging_path", std::string("vl_slam/"));
  logging_prefix_ = pnh_.param<std::string>("logging_prefix", std::string("seq_x_"));

  // Loop detection
  segmatch_loop_detection_ = pnh_.param<bool>("segmatch_loop_detection", false);
  submap_loop_detection_ = pnh_.param<bool>("submap_loop_detection", false);
  visual_loop_detection_ = pnh_.param<bool>("visual_loop_detection", false);

  // Timed callbacks.
  graph_update_interval_ = pnh_.param<double>("graph_update_interval", 1.0);
  tf_update_interval_ = pnh_.param<double>("tf_update_interval", 0.2);
  markers_update_interval_ = pnh_.param<double>("markers_update_interval", 0.15);
  map_cloud_update_interval_ = pnh_.param<double>("map_cloud_update_interval", 5.0);

  // Initialize keyframe database and loop detectors.
  initKeyframeDatabase();
  initLoopDetectors();

  // LOAM reset clients
  odometry_reset_client_ = nh_.serviceClient<std_srvs::Empty>("laserOdometry/reset");
  mapping_reset_client_ = nh_.serviceClient<std_srvs::Empty>("laserMapping/reset");
  transform_reset_client_ = nh_.serviceClient<std_srvs::Empty>("transformMaintenance/reset");
  odometry_correction_client_ = nh_.serviceClient<loam_msgs::PoseUpdate>("laserOdometry/correct_pose");
  mapping_correction_client_ = nh_.serviceClient<loam_msgs::PoseUpdate>("laserMapping/correct_pose");
  transform_correction_client_ = nh_.serviceClient<loam_msgs::PoseUpdate>("transformMaintenance/correct_pose");

  // Subscribers
  raw_cloud_sub_ = nh_.subscribe<sensor_msgs::PointCloud2>("/velodyne_points", 1, &VlSlamRos::rawCloudCallback, this);
  odom_sub_.reset(new message_filters::Subscriber<nav_msgs::Odometry>(nh_, "/loam/fixed_aft_mapped_to_init", 20));
  cloud_sub_.reset(new message_filters::Subscriber<sensor_msgs::PointCloud2>(nh_, "/loam/fixed_laser_cloud_registered", 20));
  sync_odom_cloud_.reset(new message_filters::TimeSynchronizer<nav_msgs::Odometry, sensor_msgs::PointCloud2>(*odom_sub_, *cloud_sub_, 10));
  sync_odom_cloud_->registerCallback(boost::bind(&VlSlamRos::cloudCallback, this, _1, _2));
  if (visual_loop_detection_) {
    image_sub_ = nh_.subscribe<sensor_msgs::Image>("/image/raw", 1, &VlSlamRos::imageCallback, this);
  }

  // Publishers
  odom2map_pub_ = nh_.advertise<geometry_msgs::TransformStamped>("/vl_slam/odom2map", 1, true);
  input_points_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/vl_slam/input_points", 1, true);
  map_points_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/vl_slam/map_points", 1, true);
  markers_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("/vl_slam/markers", 1, true);
  test_points_pub_ = nh_.advertise<sensor_msgs::PointCloud2>("/vl_slam/test_points", 1, true);
  test_odometry_pub_ = nh_.advertise<nav_msgs::Odometry>("/vl_slam/test_odometry", 1, true);
  test_odometry2_pub_ = nh_.advertise<nav_msgs::Odometry>("/vl_slam/test_odometry2", 1, true);

  // Services
  dump_data_service_ = nh_.advertiseService("/vl_slam/dump", &VlSlamRos::dumpDataServiceCall, this);
  save_map_service_ = nh_.advertiseService("/vl_slam/save_map", &VlSlamRos::saveMapServiceCall, this);
  log_path_service_ = nh_.advertiseService("/vl_slam/save_path_log", &VlSlamRos::logPathCallback, this);

  // Timed callbacks
  markers_publish_timer_ = nh_.createTimer(ros::Duration(markers_update_interval_), &VlSlamRos::markersTimerCallback, this);
  tf_publish_timer_ = nh_.createTimer(ros::Duration(tf_update_interval_), &VlSlamRos::publishTfTimerCallback, this);
  map_publish_timer_ = nh_.createTimer(ros::Duration(map_cloud_update_interval_), &VlSlamRos::mapPublishTimerCallback, this);
}

void VlSlamRos::rawCloudCallback(const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
  if (keyframe_db_->getNumberKeyframes() == 0) {
    return;
  }

  // Convert input cloud to PCL type.
  PointICloud::Ptr cloud(new PointICloud());
  pcl::fromROSMsg(*cloud_msg, *cloud);

  const ros::Time& stamp = cloud_msg->header.stamp;
  const Keyframe::Ptr latest_keyframe = keyframe_db_->getLatestKeyframe();
  if ((stamp - latest_keyframe->stamp_).toSec() < 0.3) {
    // Transform cloud from robot to map frame.
    const Eigen::Isometry3d pose_map = latest_keyframe->node_->estimate();
    pcl::transformPointCloud(*cloud, *cloud, pose_map.matrix());

    if (input_points_pub_.getNumSubscribers() > 0) {
      publishPointCloud(cloud, map_frame_id_, stamp, &input_points_pub_);
    }
  }
}

void VlSlamRos::cloudCallback(const nav_msgs::OdometryConstPtr& odom_msg, const sensor_msgs::PointCloud2::ConstPtr& cloud_msg) {
  // Convert input cloud to PCL type.
  PointICloud::Ptr cloud(new PointICloud());
  pcl::fromROSMsg(*cloud_msg, *cloud);

  // Get map->loam frame transform.
  const Eigen::Isometry3d T_odom = loam::convertOdometryToEigenIsometry(*odom_msg);
  const Eigen::Matrix4d inv_T_odom = T_odom.matrix().inverse();

  // Transform cloud from loam to robot frame.
  pcl::transformPointCloud(*cloud, *cloud, inv_T_odom);

  // Process cloud.
  processCloud(cloud_msg->header.stamp, T_odom, cloud);
}

void VlSlamRos::imageCallback(const sensor_msgs::Image::ConstPtr& img_msg) {
  try {
    // Get pointer to image from ROS message. Convert to OpenCV mat.
    cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvShare(img_msg, "8UC1");
    cv::Mat img = cv_ptr->image;

    // Process image.
    visual_loop_detector_->processImage(img);
  } catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
}

void VlSlamRos::publishTfTimerCallback(const ros::TimerEvent& event) {
  if (keyframe_db_->getNumberKeyframes() == 0) {
    return;
  }

  // Get pose of most recent keyframe.
  const Eigen::Isometry3d pose_map = keyframe_db_->getLatestPose();

  // Compute odom->map transform.
  Eigen::Isometry3d T_odom2map;
  ros::Time stamp;
  keyframe_db_->updateOdomToMapTransform(T_odom2map, stamp);

  // Convert transform to tf types and broadcast.
  const geometry_msgs::TransformStamped map2odom_tf =
      convertEigenMatrixToTfStamped(stamp, T_odom2map.matrix().cast<float>(), map_frame_id_, odom_frame_id_);
  const geometry_msgs::TransformStamped pose_tf =
      convertEigenMatrixToTfStamped(stamp, pose_map.matrix().cast<float>(), map_frame_id_, robot_frame_id_);
  tf_broadcaster_.sendTransform(map2odom_tf);
  tf_broadcaster_.sendTransform(pose_tf);
}

void VlSlamRos::mapPublishTimerCallback(const ros::TimerEvent& event) {
  if (map_points_pub_.getNumSubscribers() == 0 || !map_points_pub_.isLatched()) {
    return;
  }

  // Generate amd publish global map point cloud.
  pcl::PointCloud<PointI>::Ptr cloud;
  if (generateMapCloud(cloud)) {
    publishPointCloud(cloud, map_frame_id_, ros::Time::now(), &map_points_pub_);
  }
}

bool VlSlamRos::saveMapServiceCall(vl_slam_ros::SaveMapRequest& req, vl_slam_ros::SaveMapResponse& res) {
  const std::string io_msg_header = "\033[1;35m";
  ROS_INFO("%s [ Save map service ]", io_msg_header.c_str());

  const double resolution = req.resolution;
  bool success_generation;
  pcl::PointCloud<PointI>::Ptr cloud;
  if (resolution > 0.0) {
    success_generation = generateMapCloud(cloud, resolution);
  } else {
    ROS_INFO("%s    Provided resolution of %fcm is invalid", io_msg_header.c_str(), resolution);
    ROS_INFO("%s    The default resolution for publishing the map, of %fcm will be used", io_msg_header.c_str(), map_cloud_resolution_);
    success_generation = generateMapCloud(cloud);
  }

  if (success_generation) {
    cloud->header.frame_id = map_frame_id_;
    cloud->header.stamp = ros::Time::now().toNSec();
    int ret = pcl::io::savePCDFileBinary(req.destination, *cloud);
    res.success = (ret == 0);

    if (res.success == true) {
      ROS_INFO("%s    Map saved successfully", io_msg_header.c_str());
      ROS_INFO("%s    Filename: %s", io_msg_header.c_str(), req.destination.c_str());
    }
  } else {
    ROS_ERROR("%s     The map could not be saved. The keyframe list is empty.", io_msg_header.c_str());
    res.success = false;
  }
  return true;
}

bool VlSlamRos::dumpDataServiceCall(std_srvs::EmptyRequest& req, std_srvs::EmptyResponse& res) {
  const std::string io_msg_header = "\033[1;35m";
  std::cout << "\033[1;35m## -> "
            << "[ Dump service ] -> " << ros::Time::now() << "##\033[0m" << std::endl;
  {
    // Needs to be re-implemented after many changes.

    return true;
  }
}

void VlSlamRos::markersTimerCallback(const ros::TimerEvent& event) {
  if (markers_pub_.getNumSubscribers() == 0 && !markers_pub_.isLatched() || keyframe_db_->getNumberKeyframes() == 0) {
    return;
  }

  const auto markers = createMarkerArray(event.current_real);
  markers_pub_.publish(markers);
}

visualization_msgs::MarkerArray VlSlamRos::createMarkerArray(const ros::Time& stamp) {
  visualization_msgs::MarkerArray markers;
  markers.markers.resize(2);

  // node markers
  visualization_msgs::Marker& traj_marker = markers.markers[0];
  traj_marker.header.frame_id = map_frame_id_;
  traj_marker.header.stamp = stamp;
  traj_marker.ns = "icp_nodes";
  traj_marker.id = 0;
  traj_marker.type = visualization_msgs::Marker::SPHERE_LIST;

  traj_marker.pose.orientation.w = 1.0;
  traj_marker.scale.x = traj_marker.scale.y = traj_marker.scale.z = marker_scale_nodes_;

  size_t num_keyframes = keyframe_db_->getNumberKeyframes();
  traj_marker.points.resize(num_keyframes);
  traj_marker.colors.resize(num_keyframes);
  for (size_t i = 0; i < num_keyframes; i++) {
    Eigen::Vector3d pos = keyframe_db_->getKeyframe(i)->node_->estimate().translation();
    traj_marker.points[i].x = pos.x();
    traj_marker.points[i].y = pos.y();
    traj_marker.points[i].z = pos.z();

    if (i == 0) {
      traj_marker.colors[i].b = 1.0;
      traj_marker.colors[i].r = 1.0;
      traj_marker.colors[i].g = 1.0;
      traj_marker.colors[i].a = 1.0;
    } else {
      traj_marker.colors[i].r = icp_nodes_markers_color_[0];
      traj_marker.colors[i].g = icp_nodes_markers_color_[1];
      traj_marker.colors[i].b = icp_nodes_markers_color_[2];
      traj_marker.colors[i].a = icp_nodes_markers_color_[3];
    }
  }

  // edge markers
  visualization_msgs::Marker& edge_marker = markers.markers[1];
  edge_marker.header.frame_id = map_frame_id_;
  edge_marker.header.stamp = stamp;
  edge_marker.ns = "edges";
  edge_marker.id = 1;
  edge_marker.type = visualization_msgs::Marker::LINE_LIST;

  edge_marker.pose.orientation.w = 1.0;
  edge_marker.scale.x = marker_scale_edges_;

  edge_marker.points.resize(pose_graph_->optimizer->edges().size() * 2);
  edge_marker.colors.resize(pose_graph_->optimizer->edges().size() * 2);

  auto edge_itr = pose_graph_->optimizer->edges().begin();
  for (size_t i = 0; edge_itr != pose_graph_->optimizer->edges().end(); edge_itr++, i++) {
    g2o::HyperGraph::Edge* edge = *edge_itr;
    g2o::EdgeSE3* edge_se3 = dynamic_cast<g2o::EdgeSE3*>(edge);
    if (edge_se3) {
      g2o::VertexSE3* v1 = dynamic_cast<g2o::VertexSE3*>(edge_se3->vertices()[0]);
      g2o::VertexSE3* v2 = dynamic_cast<g2o::VertexSE3*>(edge_se3->vertices()[1]);
      Eigen::Vector3d pt1 = v1->estimate().translation();
      Eigen::Vector3d pt2 = v2->estimate().translation();

      edge_marker.points[i * 2].x = pt1.x();
      edge_marker.points[i * 2].y = pt1.y();
      edge_marker.points[i * 2].z = pt1.z();
      edge_marker.points[i * 2 + 1].x = pt2.x();
      edge_marker.points[i * 2 + 1].y = pt2.y();
      edge_marker.points[i * 2 + 1].z = pt2.z();

      double p1 = static_cast<double>(v1->id()) / pose_graph_->optimizer->vertices().size();
      double p2 = static_cast<double>(v2->id()) / pose_graph_->optimizer->vertices().size();

      if (std::abs(v1->id() - v2->id()) > 2) {
        edge_marker.points[i * 2].z += 0.5;
        edge_marker.points[i * 2 + 1].z += 0.5;
      }

      edge_marker.colors[i * 2].r = edges_markers_color_[0];
      edge_marker.colors[i * 2].g = edges_markers_color_[1] + p1;
      edge_marker.colors[i * 2].b = edges_markers_color_[2] - p1;
      edge_marker.colors[i * 2].a = edges_markers_color_[3];
      edge_marker.colors[i * 2 + 1].r = edges_markers_color_[0];
      edge_marker.colors[i * 2 + 1].g = edges_markers_color_[1] + p2;
      edge_marker.colors[i * 2 + 1].b = edges_markers_color_[2] - p2;
      edge_marker.colors[i * 2 + 1].a = edges_markers_color_[3];
    }
  }

  return markers;
}

bool VlSlamRos::logPathCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res) {
  logPath();

  return true;
}

}  // NAMESPACE vl_slam
