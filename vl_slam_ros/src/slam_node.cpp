
#include <memory>
#include <signal.h>
#include <thread>

#include <ros/ros.h>

#include "vl_slam_ros.hpp"

using namespace vl_slam;

// Custom signal handler, for triggering a specific shutdown routine for our node.
struct NodeSignalHandler {
  NodeSignalHandler() = delete;

  static void handleSignal(int signalValue) {
    (handler_)(signalValue);
  }

  template <class NodeType>
  static void bind(void(NodeType::*fp)(int), NodeType* node) {
    handler_ = std::bind(fp, node, std::placeholders::_1);

    signal(SIGINT, NodeSignalHandler::handleSignal);
    signal(SIGTERM, NodeSignalHandler::handleSignal);
    signal(SIGHUP, NodeSignalHandler::handleSignal);
  }

  static std::function<void(int)> handler_;
};
std::function<void(int)> NodeSignalHandler::handler_;

int main(int argc, char** argv) {
  ros::init(argc, argv, "VlSlam", ros::init_options::NoSigintHandler);
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  VlSlamRos vl_slam(nh, pnh);

  NodeSignalHandler::bind<VlSlamRos>(&VlSlamRos::shutdown, &vl_slam);

  ros::AsyncSpinner spinner(3);  // Use 10 threads
  try {
    spinner.start();
    vl_slam.startThreads();

    ros::waitForShutdown();
  } catch (const std::exception& e) {
    ROS_ERROR_STREAM("Exception: " << e.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}