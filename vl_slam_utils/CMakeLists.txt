cmake_minimum_required(VERSION 2.8.3)
project(vl_slam_utils)

set(CMAKE_CXX_STANDARD 11)
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")

find_package(Eigen3 REQUIRED QUIET)

LIST(APPEND CATKIN_DEPENDS_LIST
  pcl_catkin
  roscpp
)

find_package(catkin REQUIRED
  COMPONENTS
    ${CATKIN_DEPENDS_LIST}
)

catkin_package(
  LIBRARIES
    ${PROJECT_NAME}
  DEPENDS
    EIGEN3
  INCLUDE_DIRS
    include
  CATKIN_DEPENDS
    ${CATKIN_DEPENDS_LIST}
)

include_directories(
  include
  SYSTEM
    ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME} SHARED
  src/common.cpp
)
target_link_libraries(${PROJECT_NAME} 
  ${catkin_LIBRARIES}
)
