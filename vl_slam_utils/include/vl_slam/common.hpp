#ifndef COMMON_HPP_
#define COMMON_HPP_

#include <iostream>

#include <nav_msgs/Odometry.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>

namespace vl_slam {

typedef pcl::PointXYZ PclPoint;
typedef pcl::PointCloud<PclPoint> PointCloud;
typedef PointCloud::Ptr PointCloudPtr;

typedef pcl::PointXYZI PointI;
typedef pcl::PointCloud<PointI> PointICloud;
typedef PointICloud::Ptr PointICloudPtr;

Eigen::Matrix4d getTransformFromQuaternion(const Eigen::Quaterniond q);

// TODO doc
void publishPointCloud(const PointICloud::Ptr cloud, std::string frame_id, const ros::Time& stamp, const ros::Publisher* pub_ptr);

// TODO doc
void publishPointCloud(const PointCloud& cloud, std::string frame_id, const ros::Time& stamp, const ros::Publisher* pub_ptr);

// TODO doc
bool hasSuffix(const std::string& str, const std::string& suffix);

// TODO doc
void transformCloudToFrame(const pcl::PointCloud<PointI>::Ptr& cloud, const Eigen::Isometry3d& T_odom, const std::string frame_id);

// TODO doc
void transformCloudToFrame(const pcl::PointCloud<PointI>::Ptr& cloud, const Eigen::Matrix4d& T, const std::string frame_id);

// TODO doc
void transformCloudToFrame(pcl::PointCloud<PointI>& cloud, const Eigen::Isometry3d& T_odom, const std::string frame_id);

// TODO doc
void transformCloudToFrame(pcl::PointCloud<PointI>& cloud, const Eigen::Matrix4d& T, const std::string frame_id);

// TODO doc
nav_msgs::Odometry convertEigenIsometryToOdometry(const std::string frame_id, const Eigen::Isometry3d& odom);

template <typename PointCloudT>
static void convertToPointCloud2Msg(const PointCloudT& cloud, const std::string& frame, sensor_msgs::PointCloud2* converted) {
  // Convert to PCLPointCloud2.
  pcl::PCLPointCloud2 pcl_point_cloud_2;
  pcl::toPCLPointCloud2(cloud, pcl_point_cloud_2);
  // Convert to sensor_msgs::PointCloud2.
  pcl_conversions::fromPCL(pcl_point_cloud_2, *converted);
  // Apply frame to msg.
  converted->header.frame_id = frame;
}

}  // namespace vl_slam

#endif  // LOAM_SLAM_COMMON_HPP_
