
#include "vl_slam/common.hpp"

#include <iostream>

namespace vl_slam {

bool hasSuffix(const std::string& str, const std::string& suffix) {
  const std::size_t index = str.find(suffix, str.size() - suffix.size());
  return (index != std::string::npos);
}

void transformCloudToFrame(const pcl::PointCloud<PointI>::Ptr& cloud, const Eigen::Isometry3d& T_odom, const std::string frame_id) {
  transformCloudToFrame(cloud, T_odom.matrix(), frame_id);
}

void transformCloudToFrame(const pcl::PointCloud<PointI>::Ptr& cloud, const Eigen::Matrix4d& T, const std::string frame_id) {
  pcl::transformPointCloud(*cloud, *cloud, T);
  cloud->header.frame_id = frame_id;
}

void transformCloudToFrame(pcl::PointCloud<PointI>& cloud, const Eigen::Isometry3d& T_odom, const std::string frame_id) {
  transformCloudToFrame(cloud, T_odom.matrix(), frame_id);
}

void transformCloudToFrame(pcl::PointCloud<PointI>& cloud, const Eigen::Matrix4d& T, const std::string frame_id) {
  pcl::transformPointCloud(cloud, cloud, T);
  cloud.header.frame_id = frame_id;
}

Eigen::Matrix4d getTransformFromQuaternion(const Eigen::Quaterniond q) {
  Eigen::Matrix4d T = Eigen::Matrix4d::Identity();
  T.block<3, 3>(0, 0) = q.toRotationMatrix();
  return T;
}

void publishPointCloud(const PointICloud::Ptr cloud, std::string frame_id, const ros::Time& stamp, const ros::Publisher* pub_ptr) {
  sensor_msgs::PointCloud2Ptr cloud_msg(new sensor_msgs::PointCloud2());
  pcl::toROSMsg(*cloud, *cloud_msg);
  cloud_msg->header.frame_id = frame_id;
  cloud_msg->header.stamp = cloud_msg->header.stamp;
  pub_ptr->publish(cloud_msg);
}

void publishPointCloud(const PointCloud& cloud, std::string frame_id, const ros::Time& stamp, const ros::Publisher* pub_ptr) {
  sensor_msgs::PointCloud2Ptr cloud_msg(new sensor_msgs::PointCloud2());
  pcl::toROSMsg(cloud, *cloud_msg);
  cloud_msg->header.frame_id = frame_id;
  cloud_msg->header.stamp = cloud_msg->header.stamp;
  pub_ptr->publish(cloud_msg);
}

nav_msgs::Odometry convertEigenIsometryToOdometry(const std::string frame_id, const Eigen::Isometry3d& odom) {
  nav_msgs::Odometry odom_msg;

  const Eigen::Vector3d pos = odom.translation();
  const Eigen::Quaterniond rot(odom.rotation());

  odom_msg.header.stamp = ros::Time::now();
  odom_msg.header.frame_id = "map";
  odom_msg.pose.pose.position.x = pos.x();
  odom_msg.pose.pose.position.y = pos.y();
  odom_msg.pose.pose.position.z = pos.z();
  odom_msg.pose.pose.orientation.x = rot.x();
  odom_msg.pose.pose.orientation.y = rot.y();
  odom_msg.pose.pose.orientation.z = rot.z();
  odom_msg.pose.pose.orientation.w = rot.w();

  return odom_msg;
}

}  // namespace vl_slam
