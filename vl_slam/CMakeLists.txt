cmake_minimum_required(VERSION 2.8.3)
project(vl_slam)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -msse -msse2 -msse3 -msse4 -msse4.1 -msse4.2")

find_package(Eigen3 REQUIRED QUIET)
find_package(OpenCV 3 REQUIRED)

LIST(APPEND CATKIN_DEPENDS_LIST
  cmake_modules
  loam
  loam_msgs
  loam_ros
  pcl_catkin
  pose_graph_utils
  roscpp
  vl_slam_loop_detection
  vl_slam_utils
)

find_package(catkin REQUIRED
  COMPONENTS
    ${CATKIN_DEPENDS_LIST}
)

catkin_package(
  LIBRARIES
    ${PROJECT_NAME}
  DEPENDS
    EIGEN3
    OpenCV
  INCLUDE_DIRS
    include
    ${OpenCV_INCLUDE_DIRS}
    ${catkin_INCLUDE_DIRS}
  CATKIN_DEPENDS 
    ${CATKIN_DEPENDS_LIST}
)

include_directories(
  include
  SYSTEM
    ${OpenCV_INCLUDE_DIRS}
    ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME} SHARED
  src/vl_slam/map_cloud_generator.cpp
  src/vl_slam/graph_slam_worker.cpp
)
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
  ${OpenCV_LIBS}
  ${DLib_LIBS}
  ${DBoW2_LIBS}
)
