#include "vl_slam/map_cloud_generator.hpp"

#include <pcl/octree/octree_search.h>

namespace vl_slam {

MapCloudGenerator::MapCloudGenerator() {}

MapCloudGenerator::~MapCloudGenerator() {}

pcl::PointCloud<PointI>::Ptr MapCloudGenerator::generate(const std::vector<KeyframeSnapshot::Ptr>& keyframes, double resolution) const {
  if (keyframes.empty()) {
    return nullptr;
  }

  pcl::PointCloud<PointI>::Ptr map_cloud(new pcl::PointCloud<PointI>());
  map_cloud->reserve(keyframes.front()->cloud_->size() * keyframes.size());

  for (const auto& keyframe : keyframes) {
    Eigen::Matrix4f T_curr = keyframe->T_map_.matrix().cast<float>();
    for (const auto& src_pt : keyframe->cloud_->points) {
      PointI dst_pt;
      dst_pt.getVector4fMap() = T_curr * src_pt.getVector4fMap();
      dst_pt.intensity = src_pt.intensity;
      map_cloud->push_back(dst_pt);
    }
  }

  map_cloud->width = map_cloud->size();
  map_cloud->height = 1;
  map_cloud->is_dense = false;

  pcl::octree::OctreePointCloud<PointI> octree(resolution);
  octree.setInputCloud(map_cloud);
  octree.addPointsFromInputCloud();

  pcl::PointCloud<PointI>::Ptr filtered_map_cloud(new pcl::PointCloud<PointI>());
  octree.getOccupiedVoxelCenters(filtered_map_cloud->points);

  filtered_map_cloud->width = filtered_map_cloud->size();
  filtered_map_cloud->height = 1;
  filtered_map_cloud->is_dense = false;

  return filtered_map_cloud;
}

}  // namespace vl_slam
