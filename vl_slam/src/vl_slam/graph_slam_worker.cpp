
#include "vl_slam/graph_slam_worker.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <g2o/types/slam3d/edge_se3.h>
#include <g2o/types/slam3d/vertex_se3.h>

#include <opencv2/core.hpp>

#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>

#include <laser_slam/common.hpp>

#include <loam_msgs/PoseUpdate.h>
#include <loam_ros/common.h>

#include "vl_slam/logging.hpp"

namespace vl_slam {

GraphSlamWorker::GraphSlamWorker(ros::NodeHandle& nh, ros::NodeHandle& pnh)
    : nh_(nh), pnh_(pnh), icp_inf_matrix_(Eigen::MatrixXd::Identity(6, 6)) {}

GraphSlamWorker::~GraphSlamWorker() {}

void GraphSlamWorker::initKeyframeDatabase() {
  // Reset pointers to keyframe database, pose graph and map generator.
  keyframe_db_.reset(new KeyframeDatabase(nh_));
  pose_graph_.reset(new pose_graph_utils::PoseGraphG2O());
  map_cloud_generator_.reset(new MapCloudGenerator());
}

void GraphSlamWorker::initLoopDetectors() {
  if (segmatch_loop_detection_) {
    segmatch_loop_detector_.reset(new SegMatchLoopDetector(nh_, keyframe_db_));
  }

  if (submap_loop_detection_) {
    submap_loop_detector_.reset(new SubmapLoopDetector(nh_, keyframe_db_));
  }

  if (visual_loop_detection_) {
    visual_loop_detector_.reset(new VisualLoopDetector(nh_, keyframe_db_));
  }

  estimator_corrections_ = 0;
}

void GraphSlamWorker::startThreads() {
  graph_optimization_thread_ = std::thread(&GraphSlamWorker::optimizeGraphThread, this);

  if (visual_loop_detection_) {
    visual_loop_detector_->startThread();
  }

  if (segmatch_loop_detection_) {
    segmatch_loop_detector_->startThread();
  }
}

void GraphSlamWorker::filterInputCloud(const pcl::PointCloud<PointI>::Ptr& cloud) {
  // Voxel grid filter
  if (apply_voxel_filter_) {
    voxel_filter_.setInputCloud(cloud);
    voxel_filter_.filter(*cloud);
  }

  // Remove NaN points from point cloud.
  if (apply_nan_filter_) {
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*cloud, *cloud, indices);
  }
}

void GraphSlamWorker::processCloud(
    const ros::Time& stamp, const Eigen::Isometry3d& T_odom, const pcl::PointCloud<PointI>::Ptr& source_cloud) {
  if (!keyframe_db_->thresholdNewPose(T_odom)) {
    return;
  }

  // Apply filters to point cloud.
  filterInputCloud(source_cloud);

  // Add keyframe to database.
  addNewKeyframe(stamp, T_odom, source_cloud);

  // Send new cloud to loop detectors for place recognition.
  if (segmatch_loop_detection_) {
    segmatch_loop_detector_->processNewKeyframe(keyframe_db_->getLatestKeyframe());
  }
}

void GraphSlamWorker::addNewKeyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, const pcl::PointCloud<PointI>::Ptr& cloud) {
  const Eigen::Isometry3d T_odom2map = keyframe_db_->getTransformOdomToMap();
  const Eigen::Isometry3d pose_map = T_odom2map * T_odom;
  const double accum_dist = keyframe_db_->getAccumDistance();

  // Create keyframe instance
  Keyframe::Ptr keyframe(new Keyframe(stamp, T_odom, accum_dist, cloud));

  // Add node to the pose graph
  keyframe->node_ = pose_graph_->addSe3Node(pose_map);

  size_t num_keyframes = keyframe_db_->getNumberKeyframes();
  if (num_keyframes) {
    // Add edge between newest and previous keyframe
    const auto& prev_keyframe = keyframe_db_->getLatestKeyframe();
    const Eigen::Isometry3d relative_pose = keyframe->T_odom_.inverse() * prev_keyframe->T_odom_;
    pose_graph_->addSe3Edge(keyframe->node_, prev_keyframe->node_, relative_pose, icp_inf_matrix_);
  }

  // Finally, add keyframe to the database
  keyframe_db_->addNewKeyframe(keyframe);
}

std::vector<Loop::Ptr> GraphSlamWorker::queryLoopDetectors() {
  std::vector<Loop::Ptr> new_loops;

  // Query new loops from each detector.
  if (submap_loop_detection_) {
    std::vector<Loop::Ptr> submap_loops = submap_loop_detector_->detect();
    addLoopsToPoseGraph(submap_loops, new_loops);
  }

  if (visual_loop_detection_) {
    std::vector<Loop::Ptr> visual_loops = visual_loop_detector_->new_loops_;
    addLoopsToPoseGraph(visual_loops, new_loops);
  }

  if (segmatch_loop_detection_) {
    std::vector<Loop::Ptr> segmatch_loops = segmatch_loop_detector_->new_loops_;
    addLoopsToPoseGraph(segmatch_loops, new_loops);
  }

  // Clear list of new keyframes.
  keyframe_db_->clearNewKeyframes();

  return new_loops;
}

void GraphSlamWorker::addLoopsToPoseGraph(const std::vector<Loop::Ptr>& loops, std::vector<Loop::Ptr>& new_loops) {
  if (loops.empty()) {
    return;
  }

  // Add loop edges to pose graph.
  for (const auto& loop : loops) {
    Eigen::Isometry3d a_T_a_b = loop->estimateTransform();
    pose_graph_->addSe3Edge(loop->keyframe_a_->node_, loop->keyframe_b_->node_, Eigen::Isometry3d(a_T_a_b), loop->inf_matrix_);
  }

  // Insert new loops to global loop list.
  std::copy(loops.begin(), loops.end(), std::back_inserter(new_loops));
}

void GraphSlamWorker::runGraphOptimization(const bool& verbose) {
  std::lock_guard<std::mutex> lock(keyframe_db_->T_odom2map_mutex_);
  pose_graph_->optimize(verbose);
}

void GraphSlamWorker::optimizeGraphThread() {
  ros::Rate thread_rate(1 / graph_update_interval_);
  while (ros::ok()) {
    std::lock_guard<std::mutex> lock(main_thread_mutex_);
    if (keyframe_db_->getNumberKeyframes() < 2) {
      continue;
    }

    // Query detectors for new loops.
    std::vector<Loop::Ptr> new_loops = queryLoopDetectors();

    if (!new_loops.empty()) {
      // Add new loops to keyframes database.
      keyframe_db_->addNewLoops(new_loops);

      // Run pose graph optimization.
      runGraphOptimization(false);

      // Correct underlying estimator.
      // correctEstimator();

      // Clear lists of new loops from detectors.
      if (segmatch_loop_detection_) {
        segmatch_loop_detector_->new_loops_.clear();
        segmatch_loop_detector_->updateTrajectory();
      }
      if (visual_loop_detection_) {
        visual_loop_detector_->new_loops_.clear();
      }

      if (submap_loop_detection_) {
        submap_loop_detector_->new_loops_.clear();
      }
    } else {
      runGraphOptimization(false);
    }

    thread_rate.sleep();
  }
}

bool GraphSlamWorker::generateMapCloud(pcl::PointCloud<PointI>::Ptr& cloud, double map_cloud_resolution) {
  if (keyframe_db_->getNumberKeyframes()) {
    std::vector<KeyframeSnapshot::Ptr> snapshot = keyframe_db_->takeKeyframesSnapshot();

    cloud = map_cloud_generator_->generate(snapshot, map_cloud_resolution);

    if (cloud) {
      return true;
    }
  }
  return false;
}

bool GraphSlamWorker::generateMapCloud(pcl::PointCloud<PointI>::Ptr& cloud) {
  return generateMapCloud(cloud, map_cloud_resolution_);
}

bool GraphSlamWorker::correctEstimator() {
  // if(estimator_corrections_ == 1) return true;

  size_t num_keyframes = keyframe_db_->getNumberKeyframes();

  double dt = (ros::Time::now() - keyframe_db_->getKeyframe(num_keyframes - 1)->stamp_).toSec();
  double time_diff = (keyframe_db_->getKeyframe(num_keyframes - 1)->stamp_ - keyframe_db_->getKeyframe(num_keyframes - 2)->stamp_).toSec();
  Eigen::Isometry3d prev2_T = keyframe_db_->getKeyframe(num_keyframes - 3)->node_->estimate();
  Eigen::Isometry3d prev1_T = keyframe_db_->getKeyframe(num_keyframes - 2)->node_->estimate();
  Eigen::Isometry3d curr_T = keyframe_db_->getKeyframe(num_keyframes - 1)->node_->estimate();
  Eigen::Isometry3d rot_prev2_T = loam::rot_conv_loam.toRotationMatrix().inverse() * prev2_T;
  Eigen::Isometry3d rot_prev1_T = loam::rot_conv_loam.toRotationMatrix().inverse() * prev1_T;
  Eigen::Isometry3d rot_curr_T = loam::rot_conv_loam.toRotationMatrix().inverse() * curr_T;

  // Estimation of linear velocity with finite differences
  // First order
  // Eigen::Vector3d v = (rot_curr_T.translation() - rot_prev1_T.translation())/time_diff;
  // Second order
  Eigen::Vector3d v = (rot_curr_T.translation() - 2 * rot_prev1_T.translation() + rot_prev2_T.translation()) / (time_diff * time_diff);
  Eigen::Vector3d pos = rot_curr_T.translation();

  // Estimation of velocity in quaternions
  Eigen::Quaterniond prev1_rot(rot_prev1_T.rotation());
  Eigen::Quaterniond rot(rot_curr_T.rotation());
  Eigen::Quaterniond omega = rot * prev1_rot.inverse();

  // Estimation of next pose
  Eigen::Vector3d new_pos = pos + (v * dt);
  Eigen::Quaterniond new_rot = rot * omega;

  // Sending new transform as odometry for debugging
  Eigen::Isometry3d new_T;
  new_T.translation() = new_pos;
  new_T.linear() = new_rot.toRotationMatrix();

  // Service request construction
  loam_msgs::PoseUpdate srv;
  srv.request.pose.position.x = new_pos(0);
  srv.request.pose.position.y = new_pos(1);
  srv.request.pose.position.z = new_pos(2);
  srv.request.pose.orientation.x = rot.x();
  srv.request.pose.orientation.y = rot.y();
  srv.request.pose.orientation.z = rot.z();
  srv.request.pose.orientation.w = rot.w();

  // Call pose correction clients
  // odometry_correction_client_.call(srv);
  mapping_correction_client_.call(srv);
  transform_correction_client_.call(srv);

  ROS_ERROR("Pose correction finished");

  // Transform debugging
  // nav_msgs::Odometry test_odom_msg;
  // test_odom_msg = loam::convertEigenIsometryToOdometry(map_frame_id_, new_T);
  // test_odometry_pub_.publish(test_odom_msg);

  estimator_corrections_++;

  return true;
}

bool GraphSlamWorker::logPath() {
  ROS_INFO_STREAM("Saving trajectory logs to directory: " << logging_path_);
  ROS_INFO_STREAM("File prefix: " << logging_prefix_);

  const std::string poses_filename = logging_path_ + logging_prefix_ + "poses.txt";
  const std::string timestamps_filename = logging_path_ + logging_prefix_ + "timestamps.txt";
  const std::string submap_loops_filename = logging_path_ + logging_prefix_ + "submap_loops.txt";
  const std::string segmatch_loops_filename = logging_path_ + logging_prefix_ + "segmatch_loops.txt";
  const std::string visual_loops_filename = logging_path_ + logging_prefix_ + "visual_loops.txt";

  // Create folder to store the results
  if (!boost::filesystem::is_directory(logging_path_)) {
    if (!boost::filesystem::create_directory(logging_path_)) {
      ROS_ERROR("Directory for saving trajectory logs could not be created.");
      return false;
    }
    ROS_INFO("Directory for saving trajectory logs was successfully created.");
  }

  // Delete current files to prevent corruption of results
  std::remove(poses_filename.c_str());
  std::remove(timestamps_filename.c_str());
  std::remove(submap_loops_filename.c_str());
  std::remove(segmatch_loops_filename.c_str());
  std::remove(visual_loops_filename.c_str());

  const size_t num_keyframes = keyframe_db_->getNumberKeyframes();
  const ros::Time first_timestamp = keyframe_db_->getKeyframe(0)->stamp_;
  for (size_t i = 0; i < num_keyframes; i++) {
    const Eigen::Isometry3d T = keyframe_db_->getKeyframe(i)->node_->estimate();
    const double stamp = (keyframe_db_->getKeyframe(i)->stamp_ - first_timestamp).toSec();

    savePoseToFile(T.linear(), T.translation(), poses_filename);
    saveTimestampToFile(stamp, timestamps_filename);
  }

  const size_t num_loops = keyframe_db_->getNumberLoops();
  for (size_t i = 0; i < num_loops; i++) {
    Loop::Ptr loop = keyframe_db_->getLoop(i);

    if (loop->detector_type_ == "submap") {
      saveLoopKeysToFile(loop, submap_loops_filename);
    }
    if (loop->detector_type_ == "segmatch") {
      saveLoopKeysToFile(loop, segmatch_loops_filename);
    }
    if (loop->detector_type_ == "visualBoW") {
      saveLoopKeysToFile(loop, visual_loops_filename);
    }
  }

  return true;
}

void GraphSlamWorker::publishTestOdometry(const Eigen::Isometry3d& T, const std::string& frame_id) {
  nav_msgs::Odometry test_odom_msg;
  test_odom_msg = loam::convertEigenIsometryToOdometry(frame_id, T);
  test_odometry_pub_.publish(test_odom_msg);
}

void GraphSlamWorker::publishTestOdometry2(const Eigen::Isometry3d& T, const std::string& frame_id) {
  nav_msgs::Odometry test_odom_msg;
  test_odom_msg = loam::convertEigenIsometryToOdometry(frame_id, T);
  test_odometry2_pub_.publish(test_odom_msg);
}

void GraphSlamWorker::debugLoopTransform(const Loop::Ptr& loop, const std::string& frame_id) {
  double time_key_a = loop->keyframe_a_->stamp_.toSec();
  double time_key_b = loop->keyframe_b_->stamp_.toSec();
  Eigen::Matrix4d rel_pose = loop->rel_pose_.cast<double>();
  Eigen::Isometry3d T_key_a = loop->keyframe_a_->node_->estimate();
  Eigen::Isometry3d T_key_b = loop->keyframe_b_->node_->estimate();
  Eigen::Isometry3d T_key_a_b = T_key_b * T_key_a.inverse();

  Eigen::Isometry3d current_T_key_b;
  current_T_key_b = T_key_a_b * T_key_a;
  Eigen::Isometry3d corrected_T_key_b;
  corrected_T_key_b.matrix() = rel_pose * T_key_a_b * T_key_a.matrix();

  ROS_ERROR("Loop transform debugging");
  std::cout << "Key_a Time" << std::endl;
  std::cout << time_key_a << std::endl;
  std::cout << "Key_b Time" << std::endl;
  std::cout << time_key_b << std::endl;
  std::cout << "Key_a Pose" << std::endl;
  std::cout << T_key_a.matrix() << std::endl;
  std::cout << "Key_b Pose" << std::endl;
  std::cout << T_key_b.matrix() << std::endl;

  std::cout << "Current transform" << std::endl;
  std::cout << T_key_a_b.matrix() << std::endl;
  std::cout << "Transformed T_key_a_b pose" << std::endl;
  std::cout << current_T_key_b.matrix() << std::endl;

  std::cout << "Loop transform" << std::endl;
  std::cout << rel_pose << std::endl;
  std::cout << "Loop T_key_b pose" << std::endl;
  std::cout << corrected_T_key_b.matrix() << std::endl;

  Eigen::Matrix4d w_T_a_b = rel_pose;
  Eigen::Matrix4d T_w_a = T_key_a.matrix();
  Eigen::Matrix4d T_w_b = T_key_b.matrix();
  Eigen::Matrix4d a_T_b = T_w_a.inverse() * w_T_a_b;
  Eigen::Matrix4d a_T_a_b = a_T_b * T_w_b;
  std::cout << "w_T_a_b" << std::endl;
  std::cout << T_w_a.inverse() * w_T_a_b << std::endl;
  std::cout << "w_T_a_b * T_w_b" << std::endl;
  std::cout << w_T_a_b * T_w_b << std::endl;
  std::cout << "a_T_a_b" << std::endl;
  std::cout << a_T_a_b << std::endl;
  std::cout << "Corrected b again" << std::endl;
  std::cout << T_w_a * a_T_a_b << std::endl;

  publishTestOdometry2(Eigen::Isometry3d(a_T_a_b), "robot");
}

}  // NAMESPACE vl_slam
