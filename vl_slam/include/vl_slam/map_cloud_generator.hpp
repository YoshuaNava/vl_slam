#ifndef MAP_CLOUD_GENERATOR_HPP
#define MAP_CLOUD_GENERATOR_HPP

#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "vl_slam/common.hpp"
#include "vl_slam/keyframe.hpp"

namespace vl_slam {

/**
 * @brief This class generates a map point cloud from registered keyframes.
 */
class MapCloudGenerator {
 public:
  MapCloudGenerator();
  ~MapCloudGenerator();

  /**
   * @brief Generates a map point cloud
   * @param keyframes   Snapshot of keyframes list.
   * @param resolution  Resolution of generated map.
   * @return            Generated map point cloud.
   */
  pcl::PointCloud<PointI>::Ptr generate(const std::vector<KeyframeSnapshot::Ptr>& keyframes, double resolution) const;
};

}  // namespace vl_slam

#endif  // MAP_POINTCLOUD_GENERATOR_HPP