#ifndef GRAPH_SLAM_WORKER_HPP
#define GRAPH_SLAM_WORKER_HPP

#include <ctime>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>

#include <Eigen/Dense>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_types.h>
#include <ros/ros.h>

#include <slam_3d/pose_graph_g2o.hpp>

#include "vl_slam/common.hpp"
#include "vl_slam/keyframe_database.hpp"
#include "vl_slam/loop.hpp"
#include "vl_slam/map_cloud_generator.hpp"
#include "vl_slam/segmatch_loop_detector.hpp"
#include "vl_slam/submap_loop_detector.hpp"
#include "vl_slam/visual_loop_detector.hpp"

namespace vl_slam {

class GraphSlamWorker {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  using PoseGraph = pose_graph_utils::PoseGraphG2O;

  GraphSlamWorker(ros::NodeHandle& nh, ros::NodeHandle& pnh);

  virtual ~GraphSlamWorker();

  void initKeyframeDatabase();

  void initLoopDetectors();

  void startThreads();

 protected:
  // TODO doc
  void addNewKeyframe(const ros::Time& stamp, const Eigen::Isometry3d& T_odom, const pcl::PointCloud<PointI>::Ptr& cloud);

  // TODO doc
  void filterInputCloud(const pcl::PointCloud<PointI>::Ptr& cloud);

  // TODO doc
  void processCloud(const ros::Time& stamp, const Eigen::Isometry3d& odom, const PointICloud::Ptr& source_cloud);

  // TODO doc
  std::vector<Loop::Ptr> queryLoopDetectors();

  // TODO doc
  void addLoopsToPoseGraph(const std::vector<Loop::Ptr>& loops, std::vector<Loop::Ptr>& new_loops);

  // TODO doc
  void runGraphOptimization(const bool& verbose);

  // TODO doc
  void optimizeGraphThread();

  // TODO doc
  bool generateMapCloud(pcl::PointCloud<PointI>::Ptr& cloud, const double map_cloud_resolution);

  // TODO doc
  bool generateMapCloud(pcl::PointCloud<PointI>::Ptr& cloud);

  // TODO doc
  bool correctEstimator();

  // TODO doc
  bool logPath();

  // BEGIN debug functions
  void debugLoopTransform(const Loop::Ptr& loop, const std::string& frame_id);
  void publishTestOdometry(const Eigen::Isometry3d& T, const std::string& frame_id);
  void publishTestOdometry2(const Eigen::Isometry3d& T, const std::string& frame_id);
  // END debug functions

 protected:
  // ROS
  ros::NodeHandle nh_;
  ros::NodeHandle pnh_;

  // Frames
  std::string map_frame_id_;
  std::string odom_frame_id_;
  std::string robot_frame_id_;

  // Graph optimization
  double graph_update_interval_;
  std::thread graph_optimization_thread_;

  // Point cloud filters
  pcl::VoxelGrid<PointI> voxel_filter_;
  bool apply_voxel_filter_;
  double voxel_leaf_size_;
  bool apply_nan_filter_;

  // Map cloud generation
  double map_cloud_resolution_;
  std::unique_ptr<MapCloudGenerator> map_cloud_generator_;

  // Trajectory logging params
  std::string logging_path_;
  std::string logging_prefix_;

  // Graph slam modules
  std::mutex main_thread_mutex_;
  KeyframeDatabase::Ptr keyframe_db_;
  PoseGraph::UniquePtr pose_graph_;

  // Information matrices
  Eigen::MatrixXd icp_inf_matrix_;

  // Loop detection
  bool visual_loop_detection_;
  bool submap_loop_detection_;
  bool segmatch_loop_detection_;

  std::unique_ptr<SubmapLoopDetector> submap_loop_detector_;
  std::unique_ptr<VisualLoopDetector> visual_loop_detector_;
  std::unique_ptr<SegMatchLoopDetector> segmatch_loop_detector_;

  // Services to reset LOAM
  ros::ServiceClient odometry_reset_client_;
  ros::ServiceClient mapping_reset_client_;
  ros::ServiceClient transform_reset_client_;
  ros::ServiceClient odometry_correction_client_;
  ros::ServiceClient mapping_correction_client_;
  ros::ServiceClient transform_correction_client_;
  int estimator_corrections_;

  // Publishers for testing transformations
  ros::Publisher test_odometry_pub_;
  ros::Publisher test_odometry2_pub_;
};

}  // namespace vl_slam

#endif