#ifndef ROS_COMMON_HPP_
#define ROS_COMMON_HPP_

#include <limits>

#include <Eigen/Dense>

#include "vl_slam/keyframe.hpp"
#include "vl_slam/loop.hpp"

namespace vl_slam {

typedef std::numeric_limits<double> dbl;

static void savePoseToFile(const Eigen::Matrix3d& rot, const Eigen::Vector3d& trans, const std::string& filename) {
  std::ofstream myfile;
  myfile.open(filename, std::ios_base::app);
  myfile.precision(dbl::max_digits10);
  myfile << rot(0, 0) << " " << rot(0, 1) << " " << rot(0, 2) << " " << trans(0) << " " << rot(1, 0) << " " << rot(1, 1) << " " << rot(1, 2)
         << " " << trans(1) << " " << rot(2, 0) << " " << rot(2, 1) << " " << rot(2, 2) << " " << trans(2) << "\n";
  myfile.close();
}

static void saveTimestampToFile(const double& stamp, const std::string& filename) {
  std::ofstream myfile;
  myfile.open(filename, std::ios_base::app);
  myfile.precision(dbl::max_digits10);
  myfile << stamp << " "
         << "\n";
  myfile.close();
}

static void saveLoopKeysToFile(const Loop::Ptr loop, const std::string& filename) {
  std::ofstream myfile;

  myfile.open(filename, std::ios_base::app);
  myfile.precision(dbl::max_digits10);
  myfile << loop->stamp_.toSec() << " " << loop->keyframe_a_->node_->id() << " " << loop->keyframe_b_->node_->id() << "\n";
  myfile.close();
}

}  // namespace vl_slam

#endif  // vl_slam_ROS_COMMON_HPP_
